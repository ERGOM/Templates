#!/bin/sh
################################################################
# This shell script does the following:                        #
# - convert underlines "_" to the LaTeX underline command "\_" #
# - allow line breaks after "+", "-", "*" and ")"              #
# (both is done for each process type and for the tracers)     #
# - sorts the list of process types alphabetically             #
#                                                              #
# Author: Hagen Radtke                                         #
################################################################
<processTypes>
sed -i -e 's/_/\\_/g' <trimName>/latex_table.txt
sed -i -e 's/+/+\\allowbreak{}/g' <trimName>/latex_table.txt
sed -i -e 's/-/-\\allowbreak{}/g' <trimName>/latex_table.txt
sed -i -e 's/*/*\\allowbreak{}/g' <trimName>/latex_table.txt
sed -i -e 's/)/)\\allowbreak{}/g' <trimName>/latex_table.txt

</processTypes>
sed -i -e 's/_/\\_/g' tracer_table.txt
sed -i -e 's/+/+\\allowbreak{}/g' tracer_table.txt
sed -i -e 's/-/-\\allowbreak{}/g' tracer_table.txt
sed -i -e 's/*/*\\allowbreak{}/g' tracer_table.txt
sed -i -e 's/)/)\\allowbreak{}/g' tracer_table.txt

sed -i -e 's/_/\\_/g' tracer_equations.txt
sed -i -e 's/+/+\\allowbreak{}/g' tracer_equations.txt
sed -i -e 's/-/-\\allowbreak{}/g' tracer_equations.txt
sed -i -e 's/*/*\\allowbreak{}/g' tracer_equations.txt
sed -i -e 's/)/)\\allowbreak{}/g' tracer_equations.txt

sort processes.txt > processes_sorted.txt