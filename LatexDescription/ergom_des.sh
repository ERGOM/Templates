#!/bin/sh
# constants
sort << END_CONST |
<constants>
  <name> & <value> & <description> \\\\
</constants>
END_CONST
sed s:_:\\\\_:g > ergom_const.tex

# name
echo -n <name> > ergom_name.tex
# version
echo -n '<version>' | tr -d ' ' > ergom_version.tex
# authors
echo -n <author> > ergom_authors.tex

# 3D tracer
sort << END_TRACER |
<tracers vertLoc=WAT>
  <name> & <description> \\\\
</tracers>
END_TRACER
sed s:_:\\\\_:g > ergom_tracer.tex
if [ ! -s ergom_tracer.tex ] ; then
  echo 'no & tracer \\' > ergom_tracer.tex
fi

# 2D tracer surface
sort << END_TRACER_SURF |
<tracers vertLoc=SUR>
  <name> & <description> \\\\
</tracers>
END_TRACER_SURF
sed s:_:\\\\_:g > ergom_tracer_surf.tex
if [ ! -s ergom_tracer_surf.tex ] ; then
  echo 'no & tracer \\' > ergom_tracer_surf.tex
fi

# 2D tracer sediment
sort << END_TRACER_SED |
<tracers vertLoc=SED>
  <name> & <description> \\\\
</tracers>
END_TRACER_SED
sed s:_:\\\\_:g > ergom_tracer_sed.tex
if [ ! -s ergom_tracer_sed.tex ] ; then
  echo 'no & tracer \\' > ergom_tracer_sed.tex
fi

# virtual tracer
sort << END_TRACER_VIRT |
<tracers isActive=0>
  <name> & <description> \\\\
</tracers>
END_TRACER_VIRT
sed s:_:\\\\_:g > ergom_tracer_virt.tex
if [ ! -s ergom_tracer_virt.tex ] ; then
  echo 'no & tracer \\' > ergom_tracer_virt.tex
fi

# auxiliaries
sed s:_:\\\\_:g << END_AUX |
<auxiliaries >
             \noindent {\bf <description> :} \\\\
             \noindent $ temp1  = <temp1> $ \\\\
             \noindent $ temp2  = <temp2> $ \\\\
             \noindent $ temp3  = <temp3> $ \\\\
             \noindent $ temp4  = <temp4> $ \\\\
             \noindent $ temp5  = <temp5> $ \\\\
             \noindent $ temp6  = <temp6> $ \\\\
             \noindent $ temp7  = <temp7> $ \\\\
             \noindent $ temp8  = <temp8> $ \\\\
             \noindent $ temp9  = <temp9> $ \\\\
             \noindent $ {\bf <name>} = <formula> $ \\\\[5mm]
             
</auxiliaries>
END_AUX
cat > ergom_aux.tex

# tracer equations
#sed s:_:\\\\_:g << END_TT |
#<tracers>
#  \begin{eqnarray*}
#      \lefteqn{\frac{d\:{\bf <name>}}{dt}  =} \\\\
#      <timeTendencies>                 
#                & & <timeTendency> \\\\
#      </timeTendencies>
#  \end{eqnarray*}
#</tracers>
#END_TT
#cat > ergom_tt.tex

sed s:_:\\\\_:g << END_TT |
<tracers>
      \noindent\bigskip \[ {\frac{d\:{\bf <name>}}{dt}  =} \] \\\\
      <timeTendencies>                 
                \medskip $ <timeTendency> $ \hfill \begin{minipage}{5cm} {\small <description> \setlength{\baselineskip}{5pt}} \end{minipage} \\\\
      </timeTendencies>
</tracers>
END_TT
cat > ergom_tt.tex

# process limitations
sed s:_:\\\\_:g << END_PL |
<tracers>
  <limitations>
                \noindent \[ {\bf <name>} = <formula> \] \\\\
  </limitations>
</tracers>
END_PL
cat > ergom_pl.tex

# process rates
sed s:_:\\\\_:g << END_PR |
<processes vertLoc=WAT>
       \noindent\bigskip \[ {\bf <name> } = \] \hfill \begin{minipage}{8cm} {\small <description> }\end{minipage} \\\\
         \smallskip $ <turnover> $ \\\\               
</processes>
END_PR
cat > ergom_pr.tex

sed s:_:\\\\_:g << END_PR_SED |
<processes vertLoc=SED>
       \noindent\bigskip \[ {\bf <name> } = \] \hfill \begin{minipage}{8cm} {\small <description> }\end{minipage} \\\\
         \smallskip $ <turnover> $ \\\\               
</processes>
END_PR_SED
cat > ergom_pr_sed.tex

sed s:_:\\\\_:g << END_PR_SUR |
<processes vertLoc=SUR>
       \noindent\bigskip \[ {\bf <name> } = \] \hfill \begin{minipage}{8cm} {\small <description> }\end{minipage} \\\\
         \smallskip $ <turnover> $ \\\\               
</processes>
END_PR_SUR
cat > ergom_pr_surf.tex


