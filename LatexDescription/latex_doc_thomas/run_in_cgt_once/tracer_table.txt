\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}p{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}p{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}p{#1}}

%\definecolor{tracer}{rgb}{0.5, 1.0, 0.5}
%\definecolor{tracer1}{rgb}{0.9, 1.0, 0.9}
%\definecolor{tracer2}{rgb}{0.95, 1.0, 0.95}

% gmd requested tables without background color because online HTML
\definecolor{tracer}{rgb}{1.0, 1.0, 1.0}
\definecolor{tracer1}{rgb}{1.0, 1.0, 1.0}
\definecolor{tracer2}{rgb}{1.0, 1.0, 1.0}

\setlength\tabcolsep{0cm}

%==========================FIRST TABLE=====================
%
\begin{longtable}{L{4cm} L{12cm}}
   \hline
   \noalign{\vskip 0.2mm} 
   \multicolumn{2}{C{16cm}}{\textbf{Tracers in the water column only} \cellcolor{tracer}} \\
   \noalign{\vskip 0.2mm} 
   \hline 
   \noalign{\vskip 1mm}
\endfirsthead
   \hline
   \noalign{\vskip 0.2mm}
   \multicolumn{2}{C{16cm}}{Tracers in the water column only, continued from previous page \cellcolor{tracer}}  \\
   \noalign{\vskip 0.2mm}
   \hline
   \noalign{\vskip 1mm}
\endhead
   \noalign{\vskip 1mm}
   \hline
   \noalign{\vskip 0.2mm}
   \multicolumn{2}{C{16cm}}{continued on next page\dots \cellcolor{tracer}} \\
   \noalign{\vskip 0.2mm}
   \hline
\endfoot
   \noalign{\vskip 1mm}
   \hline
   \noalign{\vskip 0.2mm}
   \multicolumn{2}{C{16cm}}{end of table \textbf{Tracers in the water column only} \cellcolor{tracer}} \\
   \noalign{\vskip 0.2mm}
   \hline
\endlastfoot

<tracers vertLoc=WAT; isInPorewater=0>
\texttt{<trimName>} \cellcolor{tracer1} & \textbf{<description> (mol/kg)} \cellcolor{tracer1} \\
<if vertspeed/=0; vertspeedValue/=0>
vertical speed = \cellcolor{tracer2} & <vertspeedValue> m/day \cellcolor{tracer2} \\
</if>
<if vertspeed/=0; vertspeedValue=0>
vertical speed = \cellcolor{tracer2} & <vertspeed> m/day \cellcolor{tracer2} \\
</if>
<if opacity/=0>
opacity = \cellcolor{tracer2} & <opacityValue> m$^2$/mol \cellcolor{tracer2} \\
</if>
\hspace{1cm} \\
</tracers>
\end{longtable}

% ========== NEXT TABLE =======================

\begin{longtable}{L{4cm} L{12cm}}
   \hline
   \noalign{\vskip 0.2mm} 
   \multicolumn{2}{C{16cm}}{\textbf{Tracers in water and pore water} \cellcolor{tracer}} \\
   \noalign{\vskip 0.2mm} 
   \hline 
   \noalign{\vskip 1mm}
\endfirsthead
   \hline
   \noalign{\vskip 0.2mm}
   \multicolumn{2}{C{16cm}}{Tracers in water and pore water, continued from previous page \cellcolor{tracer}}  \\
   \noalign{\vskip 0.2mm}
   \hline
   \noalign{\vskip 1mm}
\endhead
   \noalign{\vskip 1mm}
   \hline
   \noalign{\vskip 0.2mm}
   \multicolumn{2}{C{16cm}}{continued on next page\dots \cellcolor{tracer}} \\
   \noalign{\vskip 0.2mm}
   \hline
\endfoot
   \noalign{\vskip 1mm}
   \hline
   \noalign{\vskip 0.2mm}
   \multicolumn{2}{C{16cm}}{end of table \textbf{Tracers in water and pore water} \cellcolor{tracer}} \\
   \noalign{\vskip 0.2mm}
   \hline
\endlastfoot

<tracers vertLoc=WAT; isInPorewater=1>
\texttt{<trimName>} \cellcolor{tracer1} & \textbf{<description> (mol/kg)} \cellcolor{tracer1} \\
<if vertspeed/=0; vertspeedValue/=0>
vertical speed = \cellcolor{tracer2} & <vertspeedValue> m/day \cellcolor{tracer2} \\
</if>
<if vertspeed/=0; vertspeedValue=0>
vertical speed = \cellcolor{tracer2} & <vertspeed> m/day \cellcolor{tracer2} \\
</if>
<if opacity/=0>
opacity = \cellcolor{tracer2} & <opacityValue> m$^2$/mol \cellcolor{tracer2} \\
</if>
\hspace{1cm} \\
</tracers>
\end{longtable}

% ========== NEXT TABLE =======================

\begin{longtable}{L{4cm} L{12cm}}
   \hline
   \noalign{\vskip 0.2mm} 
   \multicolumn{2}{C{16cm}}{\textbf{Tracers in fluff and sediment} \cellcolor{tracer}} \\
   \noalign{\vskip 0.2mm} 
   \hline 
   \noalign{\vskip 1mm}
\endfirsthead
   \hline
   \noalign{\vskip 0.2mm}
   \multicolumn{2}{C{16cm}}{Tracers in fluff and sediment, continued from previous page \cellcolor{tracer}}  \\
   \noalign{\vskip 0.2mm}
   \hline
   \noalign{\vskip 1mm}
\endhead
   \noalign{\vskip 1mm}
   \hline
   \noalign{\vskip 0.2mm}
   \multicolumn{2}{C{16cm}}{continued on next page\dots \cellcolor{tracer}} \\
   \noalign{\vskip 0.2mm}
   \hline
\endfoot
   \noalign{\vskip 1mm}
   \hline
   \noalign{\vskip 0.2mm}
   \multicolumn{2}{C{16cm}}{end of table \textbf{Tracers in fluff and sediment} \cellcolor{tracer}} \\
   \noalign{\vskip 0.2mm}
   \hline
\endlastfoot

<tracers vertLoc=SED>
\texttt{<trimName>} \cellcolor{tracer1} & \textbf{<description> (mol/m$^2$)} \cellcolor{tracer1} \\
\hspace{1cm} \\
</tracers>
\end{longtable}

%\newpage