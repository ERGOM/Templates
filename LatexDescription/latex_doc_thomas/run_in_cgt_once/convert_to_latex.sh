#!/bin/sh
################################################################
# This shell script does the following:                        #
# - convert underlines "_" to the LaTeX underline command "\_" #
# - allow line breaks after "+", "-", "*" and ")"              #
# (both is done for each process type and for the tracers)     #
# - sorts the list of process types alphabetically             #
#                                                              #
# Author: Hagen Radtke                                         #
################################################################
sed -i -e 's/_/\\_/g' gas_exchange/latex_table.txt
sed -i -e 's/+/+\\allowbreak{}/g' gas_exchange/latex_table.txt
sed -i -e 's/-/-\\allowbreak{}/g' gas_exchange/latex_table.txt
sed -i -e 's/*/*\\allowbreak{}/g' gas_exchange/latex_table.txt
sed -i -e 's/)/)\\allowbreak{}/g' gas_exchange/latex_table.txt
sed -i -e 's/\xb0\x43/\\textcelsius{}/g' gas_exchange/latex_table.txt

sed -i -e 's/_/\\_/g' BGC/pelagic/phytoplankton/latex_table.txt
sed -i -e 's/+/+\\allowbreak{}/g' BGC/pelagic/phytoplankton/latex_table.txt
sed -i -e 's/-/-\\allowbreak{}/g' BGC/pelagic/phytoplankton/latex_table.txt
sed -i -e 's/*/*\\allowbreak{}/g' BGC/pelagic/phytoplankton/latex_table.txt
sed -i -e 's/)/)\\allowbreak{}/g' BGC/pelagic/phytoplankton/latex_table.txt
sed -i -e 's/\xb0\x43/\\textcelsius{}/g' BGC/pelagic/phytoplankton/latex_table.txt

sed -i -e 's/_/\\_/g' BGC/pelagic/mineralisation/latex_table.txt
sed -i -e 's/+/+\\allowbreak{}/g' BGC/pelagic/mineralisation/latex_table.txt
sed -i -e 's/-/-\\allowbreak{}/g' BGC/pelagic/mineralisation/latex_table.txt
sed -i -e 's/*/*\\allowbreak{}/g' BGC/pelagic/mineralisation/latex_table.txt
sed -i -e 's/)/)\\allowbreak{}/g' BGC/pelagic/mineralisation/latex_table.txt
sed -i -e 's/\xb0\x43/\\textcelsius{}/g' BGC/pelagic/mineralisation/latex_table.txt

sed -i -e 's/_/\\_/g' BGC/pelagic/zooplankton/latex_table.txt
sed -i -e 's/+/+\\allowbreak{}/g' BGC/pelagic/zooplankton/latex_table.txt
sed -i -e 's/-/-\\allowbreak{}/g' BGC/pelagic/zooplankton/latex_table.txt
sed -i -e 's/*/*\\allowbreak{}/g' BGC/pelagic/zooplankton/latex_table.txt
sed -i -e 's/)/)\\allowbreak{}/g' BGC/pelagic/zooplankton/latex_table.txt
sed -i -e 's/\xb0\x43/\\textcelsius{}/g' BGC/pelagic/zooplankton/latex_table.txt

sed -i -e 's/_/\\_/g' BGC/pelagic/reoxidation/latex_table.txt
sed -i -e 's/+/+\\allowbreak{}/g' BGC/pelagic/reoxidation/latex_table.txt
sed -i -e 's/-/-\\allowbreak{}/g' BGC/pelagic/reoxidation/latex_table.txt
sed -i -e 's/*/*\\allowbreak{}/g' BGC/pelagic/reoxidation/latex_table.txt
sed -i -e 's/)/)\\allowbreak{}/g' BGC/pelagic/reoxidation/latex_table.txt
sed -i -e 's/\xb0\x43/\\textcelsius{}/g' BGC/pelagic/reoxidation/latex_table.txt

sed -i -e 's/_/\\_/g' BGC/benthic/mineralisation/latex_table.txt
sed -i -e 's/+/+\\allowbreak{}/g' BGC/benthic/mineralisation/latex_table.txt
sed -i -e 's/-/-\\allowbreak{}/g' BGC/benthic/mineralisation/latex_table.txt
sed -i -e 's/*/*\\allowbreak{}/g' BGC/benthic/mineralisation/latex_table.txt
sed -i -e 's/)/)\\allowbreak{}/g' BGC/benthic/mineralisation/latex_table.txt
sed -i -e 's/\xb0\x43/\\textcelsius{}/g' BGC/benthic/mineralisation/latex_table.txt

sed -i -e 's/_/\\_/g' BGC/benthic/P_retention/latex_table.txt
sed -i -e 's/+/+\\allowbreak{}/g' BGC/benthic/P_retention/latex_table.txt
sed -i -e 's/-/-\\allowbreak{}/g' BGC/benthic/P_retention/latex_table.txt
sed -i -e 's/*/*\\allowbreak{}/g' BGC/benthic/P_retention/latex_table.txt
sed -i -e 's/)/)\\allowbreak{}/g' BGC/benthic/P_retention/latex_table.txt
sed -i -e 's/\xb0\x43/\\textcelsius{}/g' BGC/benthic/P_retention/latex_table.txt

sed -i -e 's/_/\\_/g' physics/sedimentation/latex_table.txt
sed -i -e 's/+/+\\allowbreak{}/g' physics/sedimentation/latex_table.txt
sed -i -e 's/-/-\\allowbreak{}/g' physics/sedimentation/latex_table.txt
sed -i -e 's/*/*\\allowbreak{}/g' physics/sedimentation/latex_table.txt
sed -i -e 's/)/)\\allowbreak{}/g' physics/sedimentation/latex_table.txt
sed -i -e 's/\xb0\x43/\\textcelsius{}/g' physics/sedimentation/latex_table.txt

sed -i -e 's/_/\\_/g' physics/erosion/latex_table.txt
sed -i -e 's/+/+\\allowbreak{}/g' physics/erosion/latex_table.txt
sed -i -e 's/-/-\\allowbreak{}/g' physics/erosion/latex_table.txt
sed -i -e 's/*/*\\allowbreak{}/g' physics/erosion/latex_table.txt
sed -i -e 's/)/)\\allowbreak{}/g' physics/erosion/latex_table.txt
sed -i -e 's/\xb0\x43/\\textcelsius{}/g' physics/erosion/latex_table.txt

sed -i -e 's/_/\\_/g' BGC/benthic/bioresuspension/latex_table.txt
sed -i -e 's/+/+\\allowbreak{}/g' BGC/benthic/bioresuspension/latex_table.txt
sed -i -e 's/-/-\\allowbreak{}/g' BGC/benthic/bioresuspension/latex_table.txt
sed -i -e 's/*/*\\allowbreak{}/g' BGC/benthic/bioresuspension/latex_table.txt
sed -i -e 's/)/)\\allowbreak{}/g' BGC/benthic/bioresuspension/latex_table.txt
sed -i -e 's/\xb0\x43/\\textcelsius{}/g' BGC/benthic/bioresuspension/latex_table.txt

sed -i -e 's/_/\\_/g' physics/parametrization_deep_burial/latex_table.txt
sed -i -e 's/+/+\\allowbreak{}/g' physics/parametrization_deep_burial/latex_table.txt
sed -i -e 's/-/-\\allowbreak{}/g' physics/parametrization_deep_burial/latex_table.txt
sed -i -e 's/*/*\\allowbreak{}/g' physics/parametrization_deep_burial/latex_table.txt
sed -i -e 's/)/)\\allowbreak{}/g' physics/parametrization_deep_burial/latex_table.txt
sed -i -e 's/\xb0\x43/\\textcelsius{}/g' physics/parametrization_deep_burial/latex_table.txt

sed -i -e 's/_/\\_/g' standard/latex_table.txt
sed -i -e 's/+/+\\allowbreak{}/g' standard/latex_table.txt
sed -i -e 's/-/-\\allowbreak{}/g' standard/latex_table.txt
sed -i -e 's/*/*\\allowbreak{}/g' standard/latex_table.txt
sed -i -e 's/)/)\\allowbreak{}/g' standard/latex_table.txt
sed -i -e 's/\xb0\x43/\\textcelsius{}/g' standard/latex_table.txt

sed -i -e 's/_/\\_/g' tracer_table.txt
sed -i -e 's/+/+\\allowbreak{}/g' tracer_table.txt
sed -i -e 's/-/-\\allowbreak{}/g' tracer_table.txt
sed -i -e 's/*/*\\allowbreak{}/g' tracer_table.txt
sed -i -e 's/)/)\\allowbreak{}/g' tracer_table.txt
sed -i -e 's/\xb0\x43/\\textcelsius{}/g' tracer_table.txt

sed -i -e 's/_/\\_/g' tracer_equations.txt
sed -i -e 's/+/+\\allowbreak{}/g' tracer_equations.txt
sed -i -e 's/-/-\\allowbreak{}/g' tracer_equations.txt
sed -i -e 's/*/*\\allowbreak{}/g' tracer_equations.txt
sed -i -e 's/)/)\\allowbreak{}/g' tracer_equations.txt
sed -i -e 's/\xb0\x43/\\textcelsius{}/g' tracer_equations.txt

sort processes.txt > processes_sorted.txt
