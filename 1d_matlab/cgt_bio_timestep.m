    % calculate total element concentrations in the water column
    for k = 1:kmax
          <celements>
             tracer_vector_<total>(k) = ...
             <containingTracers vertLoc=WAT>
                max(0.0,tracer_vector_<ct>(k))*<ctAmount> + ...
             </containingTracers>
                0.0;
          </celements>   
    end

    % calculate total colored element concentrations at bottom
       <celements>
          tracer_scalar_<totalBottom> = ...
          <containingTracers vertLoc=SED>
             max(0.0,tracer_scalar_<ct>)*<ctAmount> + ...
          </containingTracers>
             0.0;
       </celements>    

    % First, do the Pre-Loop to calculate isZIntegral=1 auxiliaries

    % initialize isZIntegral auxiliary variables with zero
    <auxiliaries isZIntegral=1; calcAfterProcesses=0>
       <name> = 0.0; 
    </auxiliaries>

    mydate=datevec(current_date);
    cgt_year=mydate(1);
    if eomday(mydate(1),mydate(2))==29 
      if mydate(2)==1 cgt_dayofyear=mydate(3); end
      if mydate(3)==2 cgt_dayofyear=mydate(3)+31; end
      if mydate(3)==3 cgt_dayofyear=mydate(3)+60; end
      if mydate(3)==4 cgt_dayofyear=mydate(3)+91; end
      if mydate(3)==5 cgt_dayofyear=mydate(3)+121; end
      if mydate(3)==6 cgt_dayofyear=mydate(3)+152; end
      if mydate(3)==7 cgt_dayofyear=mydate(3)+182; end
      if mydate(3)==8 cgt_dayofyear=mydate(3)+213; end
      if mydate(3)==9 cgt_dayofyear=mydate(3)+244; end
      if mydate(3)==10 cgt_dayofyear=mydate(3)+274; end
      if mydate(3)==11 cgt_dayofyear=mydate(3)+305; end
      if mydate(3)==12 cgt_dayofyear=mydate(3)+335; end
    else
      if mydate(2)==1 cgt_dayofyear=mydate(3); end
      if mydate(3)==2 cgt_dayofyear=mydate(3)+31; end
      if mydate(3)==3 cgt_dayofyear=mydate(3)+59; end
      if mydate(3)==4 cgt_dayofyear=mydate(3)+90; end
      if mydate(3)==5 cgt_dayofyear=mydate(3)+120; end
      if mydate(3)==6 cgt_dayofyear=mydate(3)+151; end
      if mydate(3)==7 cgt_dayofyear=mydate(3)+181; end
      if mydate(3)==8 cgt_dayofyear=mydate(3)+212; end
      if mydate(3)==9 cgt_dayofyear=mydate(3)+243; end
      if mydate(3)==10 cgt_dayofyear=mydate(3)+273; end
      if mydate(3)==11 cgt_dayofyear=mydate(3)+304; end
      if mydate(3)==12 cgt_dayofyear=mydate(3)+334; end
    end
    cgt_hour=mydate(4)+mydate(5)/60+mydate(6)/3600;

    cgt_bottomdepth = 0.0;
    for k = 1:kmax
       cgt_bottomdepth = cgt_bottomdepth + cellheights(k);
          
             %------------------------------------
             % STEP 0.1: prepare abiotic parameters
             %------------------------------------
             cgt_temp       = forcing_vector_temperature(k);           % potential temperature     [Celsius]
             cgt_sali       = forcing_vector_salinity(k);              % salinity                  [g/kg]
             cgt_light      = forcing_vector_light(k);                 % light intensity           [W/m2]
             cgt_cellheight = cellheights(k);                          % cell height               [m]
             cgt_density    = density_water;                           % density                   [kg/m3]
             cgt_timestep   = timestep;                                % timestep                  [days]
             cgt_longitude  = location.longitude;                      % geographic longitude      [deg]
             cgt_latitude   = location.latitude;                       % geographic latitude       [deg]
             if k == kmax 
                cgt_current_wave_stress=forcing_scalar_bottom_stress;  % bottom stress             [N/m2]
             end                     
             
             %------------------------------------
             % STEP 0.2: load tracer values
             %------------------------------------
<tracers vertLoc=WAT; calcBeforeZIntegral=1>
             <name> = tracer_vector_<name>(k); % <description>
             if k == 1
                above_<name> = tracer_vector_<name>(k);
             else
                above_<name> = tracer_vector_<name>(k-1);
             end
</tracers>
<tracers vertLoc=FIS; calcBeforeZIntegral=1>
             <name> = tracer_scalar_<name>; % <description>
</tracers>             
<auxiliaries vertLoc=WAT; isUsedElsewhere=1; calcBeforeZIntegral=1>
             <name> = auxiliary_vector_<name>(k); % <description>
</auxiliaries>

<tracers vertLoc=WAT; isPositive=1; calcBeforeZIntegral=1>
             <name>       = max(<name>,0.0);
             above_<name> = max(above_<name>,0.0);
</tracers>
<tracers vertLoc=FIS; isPositive=1; calcBeforeZIntegral=1>
             <name>       = max(<name>,0.0);
</tracers>

             if k == kmax
<tracers vertLoc=SED; calcBeforeZIntegral=1>
                <name> = tracer_scalar_<name>; % <description>
</tracers>

<tracers vertLoc=SED; isPositive=1; calcBeforeZIntegral=1>
                <name> = max(<name>,0.0);
</tracers>
             end

             %------------------------------------
             % STEP 0.3: calculate auxiliaries
             %------------------------------------
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=1; calcBeforeZIntegral=1>
             % <description> :
             <name> = (above_<formula>-<formula>)/cgt_cellheight;

</auxiliaries>
          
             % initialize auxiliaries for iterative loop
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; calcBeforeZIntegral=1; iterations/=0>
             <name> = <iterInit>;
</auxiliaries>

             % iterative loop follows
             for cgt_iteration=1:<maxIterations>
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; calcBeforeZIntegral=1; iterations/=0>
               % <description> :
               if cgt_iteration <= <iterations>
                 temp1  = <temp1>;
                 temp2  = <temp2>;
                 temp3  = <temp3>;
                 temp4  = <temp4>;
                 temp5  = <temp5>;
                 temp6  = <temp6>;
                 temp7  = <temp7>;
                 temp8  = <temp8>;
                 temp9  = <temp9>;
                 <name> = <formula>;
               end
</auxiliaries>
             end

<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; calcBeforeZIntegral=1; iterations=0>
             % <description> :
             temp1  = <temp1>;
             temp2  = <temp2>;
             temp3  = <temp3>;
             temp4  = <temp4>;
             temp5  = <temp5>;
             temp6  = <temp6>;
             temp7  = <temp7>;
             temp8  = <temp8>;
             temp9  = <temp9>;
             <name> = <formula>;
             
</auxiliaries>

             if k == kmax
                % initialize auxiliaries for iterative loop
<auxiliaries vertLoc=SED; calcAfterProcesses=0; isZIntegral=0; calcBeforeZIntegral=1; iterations/=0>
                <name> = <iterInit>;
</auxiliaries>

                % iterative loop follows
                for cgt_iteration=1:<maxIterations>
<auxiliaries vertLoc=SED; calcAfterProcesses=0; isZIntegral=0; calcBeforeZIntegral=1; iterations/=0>
                  % <description> :
                  if cgt_iteration <= <iterations>
                    temp1  = <temp1>;
                    temp2  = <temp2>;
                    temp3  = <temp3>;
                    temp4  = <temp4>;
                    temp5  = <temp5>;
                    temp6  = <temp6>;
                    temp7  = <temp7>;
                    temp8  = <temp8>;
                    temp9  = <temp9>;
                    <name> = <formula>;
                  end
</auxiliaries>
                end

<auxiliaries vertLoc=SED; calcAfterProcesses=0; calcBeforeZIntegral=1; isZIntegral=0; iterations=0>
                % <description> :
                temp1  = <temp1>;
                temp2  = <temp2>;
                temp3  = <temp3>;
                temp4  = <temp4>;
                temp5  = <temp5>;
                temp6  = <temp6>;
                temp7  = <temp7>;
                temp8  = <temp8>;
                temp9  = <temp9>;
                <name> = <formula> ;
                
</auxiliaries>
             end
             
             if k == 1
                % initialize auxiliaries for iterative loop
<auxiliaries vertLoc=SUR; calcAfterProcesses=0; calcBeforeZIntegral=1; iterations/=0>
                <name> = <iterInit>;
</auxiliaries>

                % iterative loop follows
                for cgt_iteration=1:<maxIterations>
<auxiliaries vertLoc=SUR; calcAfterProcesses=0; calcBeforeZIntegral=1; iterations/=0>
                  % <description> :
                  if cgt_iteration <= <iterations>
                    temp1  = <temp1>;
                    temp2  = <temp2>;
                    temp3  = <temp3>;
                    temp4  = <temp4>;
                    temp5  = <temp5>;
                    temp6  = <temp6>;
                    temp7  = <temp7>;
                    temp8  = <temp8>;
                    temp9  = <temp9>;
                    <name> = <formula>;
                  end
</auxiliaries>
                end

<auxiliaries vertLoc=SUR; calcAfterProcesses=0; calcBeforeZIntegral=1>
                % <description> :
                temp1  = <temp1>;
                temp2  = <temp2>;
                temp3  = <temp3>;
                temp4  = <temp4>;
                temp5  = <temp5>;
                temp6  = <temp6>;
                temp7  = <temp7>;
                temp8  = <temp8>;
                temp9  = <temp9>;                
                <name> = <formula> ;
                
</auxiliaries>
             end
             
             %------------------------------------
             % STEP 0.4: add contribution of the current layer k to the zIntegral
             %------------------------------------
<auxiliaries isZIntegral=1; calcAfterProcesses=0>             
             <name> = <name> + (<formula>)*cgt_cellheight*cgt_density;
</auxiliaries>
    end

    % initialize isZIntegral auxiliary variables with zero
<auxiliaries isZIntegral=1; calcAfterProcesses=1>
    <name> = 0.0; 
</auxiliaries>


    %initialize the variable which cumulates the change of the vertLoc=FIS tracer in each k layer with zero
<tracers vertLoc=FIS>
    cumulated_change_of_<name> = 0.0;
</tracers>

    % Main loop follows

    cgt_bottomdepth = 0.0;
    for k = 1:kmax
       cgt_bottomdepth = cgt_bottomdepth + cellheights(k);
          
             %------------------------------------
             % STEP 1: prepare abiotic parameters
             %------------------------------------
             cgt_temp       = forcing_vector_temperature(k);           % potential temperature     [Celsius]
             cgt_sali       = forcing_vector_salinity(k);              % salinity                  [g/kg]
             cgt_light      = forcing_vector_light(k);                 % light intensity           [W/m2]
             cgt_cellheight = cellheights(k);                          % cell height               [m]
             cgt_density    = density_water;                           % density                   [kg/m3]
             cgt_timestep   = timestep;                                % timestep                  [days]
             cgt_latitude   = location.latitude;                       % geographic latitude       [deg]
             cgt_longitude  = location.longitude;                      % geographic longitude      [deg]
             if k == kmax 
                cgt_current_wave_stress=forcing_scalar_bottom_stress;  % bottom stress             [N/m2]
             end                     
             
             %------------------------------------
             % STEP 2: load tracer values
             %------------------------------------
<tracers vertLoc=WAT>
             <name> = tracer_vector_<name>(k); % <description>
             if k == 1
                above_<name> = tracer_vector_<name>(k);
             else
                above_<name> = tracer_vector_<name>(k-1);
             end
</tracers>             
<tracers vertLoc=FIS>
             <name> = tracer_scalar_<name>; % <description>
</tracers>             

<auxiliaries vertLoc=WAT; isUsedElsewhere=1>
             <name> = auxiliary_vector_<name>(k); % <description>
</auxiliaries>

<tracers vertLoc=WAT; isPositive=1>
             <name>       = max(<name>,0.0);
             above_<name> = max(above_<name>,0.0);
</tracers>
<tracers vertLoc=FIS; isPositive=1>
             <name>       = max(<name>,0.0);
</tracers>


             if k == kmax
<tracers vertLoc=SED>
                <name> = tracer_scalar_<name>; % <description>
</tracers>

<tracers vertLoc=SED; isPositive=1>
                <name> = max(<name>,0.0);
</tracers>
             end

             %------------------------------------
             % STEP 4.1: calculate auxiliaries
             %------------------------------------
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=1>
             % <description> :
             <name> = (above_<formula>-<formula>)/cgt_cellheight;

</auxiliaries>
          
             % initialize auxiliaries for iterative loop
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; iterations/=0>
             <name> = <iterInit>;
</auxiliaries>

             % iterative loop follows
             for cgt_iteration=1:<maxIterations>
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; iterations/=0>
               % <description> :
               if cgt_iteration <= <iterations>
                 temp1  = <temp1>;
                 temp2  = <temp2>;
                 temp3  = <temp3>;
                 temp4  = <temp4>;
                 temp5  = <temp5>;
                 temp6  = <temp6>;
                 temp7  = <temp7>;
                 temp8  = <temp8>;
                 temp9  = <temp9>;
                 <name> = <formula>;
               end
</auxiliaries>
             end

<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; iterations=0>
             % <description> :
             temp1  = <temp1>;
             temp2  = <temp2>;
             temp3  = <temp3>;
             temp4  = <temp4>;
             temp5  = <temp5>;
             temp6  = <temp6>;
             temp7  = <temp7>;
             temp8  = <temp8>;
             temp9  = <temp9>;
             <name> = <formula>;
             
</auxiliaries>

             if k == kmax
                % initialize auxiliaries for iterative loop
<auxiliaries vertLoc=SED; calcAfterProcesses=0; isZIntegral=0; iterations/=0>
                <name> = <iterInit>;
</auxiliaries>

                % iterative loop follows
                for cgt_iteration=1:<maxIterations>
<auxiliaries vertLoc=SED; calcAfterProcesses=0; isZIntegral=0; iterations/=0>
                  % <description> :
                  if cgt_iteration <= <iterations>
                    temp1  = <temp1>;
                    temp2  = <temp2>;
                    temp3  = <temp3>;
                    temp4  = <temp4>;
                    temp5  = <temp5>;
                    temp6  = <temp6>;
                    temp7  = <temp7>;
                    temp8  = <temp8>;
                    temp9  = <temp9>;
                    <name> = <formula>;
                  end
</auxiliaries>
                end

<auxiliaries vertLoc=SED; calcAfterProcesses=0; isZIntegral=0; iterations=0>
                % <description> :
                temp1  = <temp1>;
                temp2  = <temp2>;
                temp3  = <temp3>;
                temp4  = <temp4>;
                temp5  = <temp5>;
                temp6  = <temp6>;
                temp7  = <temp7>;
                temp8  = <temp8>;
                temp9  = <temp9>;
                <name> = <formula> ;
                
</auxiliaries>
             end
             
             if k == 1
                % initialize auxiliaries for iterative loop
<auxiliaries vertLoc=SUR; calcAfterProcesses=0; iterations/=0>
                <name> = <iterInit>;
</auxiliaries>

                % iterative loop follows
                for cgt_iteration=1:<maxIterations>
<auxiliaries vertLoc=SUR; calcAfterProcesses=0; iterations/=0>
                  % <description> :
                  if cgt_iteration <= <iterations>
                    temp1  = <temp1>;
                    temp2  = <temp2>;
                    temp3  = <temp3>;
                    temp4  = <temp4>;
                    temp5  = <temp5>;
                    temp6  = <temp6>;
                    temp7  = <temp7>;
                    temp8  = <temp8>;
                    temp9  = <temp9>;
                    <name> = <formula>;
                  end
</auxiliaries>
                end

<auxiliaries vertLoc=SUR; calcAfterProcesses=0; iterations=0>
                % <description> :
                temp1  = <temp1>;
                temp2  = <temp2>;
                temp3  = <temp3>;
                temp4  = <temp4>;
                temp5  = <temp5>;
                temp6  = <temp6>;
                temp7  = <temp7>;
                temp8  = <temp8>;
                temp9  = <temp9>;                
                <name> = <formula> ;
                
</auxiliaries>
             end
             
             %------------------------------------
             % STEP 4.2: output of auxiliaries
             %------------------------------------
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isOutput=1>             
             output_vector_<name>(k) = output_vector_<name>(k) + <name>;
</auxiliaries>

             if k == kmax
<auxiliaries vertLoc=SED; calcAfterProcesses=0; isOutput=1>
                   output_scalar_<name> = output_scalar_<name> + <name>;
</auxiliaries>
             end
             if k == 1
<auxiliaries vertLoc=SUR; calcAfterProcesses=0; isOutput=1>
                   output_scalar_<name> = output_scalar_<name> + <name>;
</auxiliaries>
             end

             %------------------------------------
             % STEP 5: calculate process limitations
             %------------------------------------

<tracers vertLoc=WAT>
  <limitations>
             <name> = <formula>;
  </limitations>
</tracers>

             if k == kmax
<tracers vertLoc=SED>
  <limitations>
                <name> = <formula>;
  </limitations>
</tracers>
             end

             if k == 1
<tracers vertLoc=SUR>
  <limitations>
                <name> = <formula>;
  </limitations>
</tracers>
             end

             %------------------------------------
             %-- POSITIVE-DEFINITE SCHEME --------
             %-- means the following steps will be repeated as often as nessecary
             %------------------------------------

             fraction_of_total_timestep = 1.0;   % how much of the original timestep is remaining
<processes>
             total_rate_<name>          = 0.0;
</processes>
             number_of_loop = 1;

             while cgt_timestep > 0.0

                %------------------------------------
                % STEP 6.1: calculate process rates
                %------------------------------------
<processes vertLoc=WAT>
                % <description> :
                <name> = <turnover>;
                <name> = max(<name>,0.0);

</processes>

                if k == kmax
<processes vertLoc=SED>
                   % <description> :
                   <name> = <turnover>;
                   <name> = max(<name>,0.0);
                
</processes>
                end
             
                if k == 1
<processes vertLoc=SUR>
                   % <description> :
                   <name> = <turnover>;
                   <name> = max(<name>,0.0);
                
</processes>
                end

                %------------------------------------
                % STEP 6.2: calculate possible euler-forward change (in a full timestep)
                %------------------------------------

<tracers>
                change_of_<name> = 0.0;
</tracers>

<tracers vertLoc=WAT; hasTimeTendenciesVertLoc=WAT>
             
                change_of_<name> = change_of_<name> + cgt_timestep*(0.0 ...
                <timeTendencies vertLoc=WAT>
                   <timeTendency> ... % <description>
                </timeTendencies>
                );
</tracers>
<tracers vertLoc=FIS; hasTimeTendenciesVertLoc=WAT>
             
                change_of_<name> = change_of_<name> + cgt_timestep*(0.0 ...
                <timeTendencies vertLoc=WAT>
                   <timeTendency> ... % <description>
                </timeTendencies>
                );
</tracers>

                if k == 1
<tracers vertLoc=WAT; hasTimeTendenciesVertLoc=SUR>

                   change_of_<name> = change_of_<name> + cgt_timestep*(0.0 ...
                   <timeTendencies vertLoc=SUR>
                      <timeTendency> ... % <description>
                   </timeTendencies>
                   );
</tracers>
                end

                if k == kmax
<tracers vertLoc=WAT; hasTimeTendenciesVertLoc=SED>

                   change_of_<name> = change_of_<name> + cgt_timestep*(0.0 ...
                   <timeTendencies vertLoc=SED>
                      <timeTendency> ... % <description>
                   </timeTendencies>
                   );
</tracers>
<tracers vertLoc=SED; hasTimeTendencies>

                   change_of_<name> = change_of_<name> + cgt_timestep*(0.0 ...
                   <timeTendencies>
                      <timeTendency> ... % <description>
                   </timeTendencies>
                   );
</tracers>                         
<tracers vertLoc=FIS; hasTimeTendenciesVertLoc=SED>

                   change_of_<name> = change_of_<name> + cgt_timestep*(0.0 ...
                   <timeTendencies vertLoc=SED>
                      <timeTendency> ... % <description>
                   </timeTendencies>
                   );
</tracers>

                end           

                %------------------------------------
                % STEP 6.3: calculate maximum fraction of the timestep before some tracer gets exhausted
                %------------------------------------

                timestep_fraction = 1.0;
                which_tracer_exhausted = -1;

                % find the tracer which is exhausted after the shortest period of time

                % in the water column
<tracers vertLoc=WAT; isPositive=1>
             
                % check if tracer <name> was exhausted from the beginning and is still consumed
                if (tracer_vector_<name>(k) <= 0.0) && (change_of_<name> < 0.0)
                   timestep_fraction = 0.0;
                   which_tracer_exhausted = <numTracer>;
                end
                % check if tracer <name> was present, but got exhausted
                if (tracer_vector_<name>(k) > 0.0) && (tracer_vector_<name>(k) + change_of_<name> < 0.0)
                   timestep_fraction_new = tracer_vector_<name>(k) / (0.0 - change_of_<name>);
                   if timestep_fraction_new <= timestep_fraction
                      which_tracer_exhausted = <numTracer>;
                      timestep_fraction = timestep_fraction_new;
                   end
                end
</tracers>
          
                % in the bottom layer
                if k == kmax
<tracers vertLoc=SED; isPositive=1>

                   % check if tracer <name> was exhausted from the beginning and is still consumed
                   if (tracer_scalar_<name> <= 0.0) && (change_of_<name> < 0.0)
                      timestep_fraction = 0.0;
                      which_tracer_exhausted = <numTracer>;
                   end
                   % check if tracer <name> was present, but got exhausted
                   if (tracer_scalar_<name> > 0.0) && (tracer_scalar_<name> + change_of_<name> < 0.0)
                      timestep_fraction_new = tracer_scalar_<name> / (0.0 - change_of_<name>);
                      if timestep_fraction_new <= timestep_fraction
                         which_tracer_exhausted = <numTracer>;
                         timestep_fraction = timestep_fraction_new;
                      end
                   end
</tracers>                         
                end          

                % now, update the limitations: rates of the processes limited by this tracer become zero in the future

<tracers isPositive=1; vertLoc/=FIS>
                if <numTracer> == which_tracer_exhausted
                  <limitations>
                   <name> = 0.0;
                  </limitations>
                end
</tracers>

                %------------------------------------
                % STEP 6.4: apply a Euler-forward timestep with the fraction of the time
                %------------------------------------ 

                % in the water column
<tracers vertLoc=WAT>
             
                % tracer <name> (<description>):
                tracer_vector_<name>(k) = tracer_vector_<name>(k) + change_of_<name> * timestep_fraction;
</tracers>
<tracers vertLoc=FIS>
             
                % tracer <name> (<description>):
                cumulated_change_of_<name> = cumulated_change_of_<name> + change_of_<name> * timestep_fraction;
</tracers>

          
                % in the bottom layer
                if k == kmax
<tracers vertLoc=SED>

                   % tracer <name> (<description>)
                   tracer_scalar_<name> = tracer_scalar_<name> + change_of_<name> * timestep_fraction;
</tracers>                         
                end           

                %------------------------------------
                % STEP 6.5: output of process rates
                %------------------------------------
<processes vertLoc=WAT; isOutput=1>             
                output_vector_<name>(k) = output_vector_<name>(k) + <name> * timestep_fraction * fraction_of_total_timestep;
</processes>
                if k == kmax
<processes vertLoc=SED; isOutput=1>
                   output_scalar_<name> = output_scalar_<name> + <name> * timestep_fraction * fraction_of_total_timestep;
</processes>
                end
                if k == 1
<processes vertLoc=SUR; isOutput=1>
                   output_scalar_<name> = output_scalar_<name> + <name> * timestep_fraction * fraction_of_total_timestep;
</processes>
                end
             
                %------------------------------------
                % STEP 6.6: set timestep to remaining timestep only
                %------------------------------------

                cgt_timestep = cgt_timestep * (1.0 - timestep_fraction);                         % remaining timestep
                fraction_of_total_timestep = fraction_of_total_timestep * (1.0 - timestep_fraction); % how much of the original timestep is remaining


                if number_of_loop > 100
                   error('aborted positive-definite scheme: more than 100 iterations');
                end
                number_of_loop=number_of_loop+1;

             end
             %------------------------------------
             %-- END OF POSITIVE-DEFINITE SCHEME -
             %------------------------------------  

             %------------------------------------
             % STEP 7.0: add cumulated change to vertLoc=FIS tracers
             %------------------------------------
             
             if k == kmax
<tracers vertLoc=FIS>
               % apply change of <description>:
               tracer_scalar_<name> = tracer_scalar_<name> + cumulated_change_of_<name>;
               <name> = tracer_scalar_<name>;
</tracers>
             end

             %------------------------------------
             % STEP 7.1: output of new tracer concentrations
             %------------------------------------
<tracers vertLoc=WAT; isOutput=1>
             output_vector_<name>(k) = output_vector_<name>(k) + <name>;
</tracers>
             if k == kmax
<tracers vertLoc=SED; isOutput=1>
               output_scalar_<name> = output_scalar_<name> + <name>;
</tracers>
<tracers vertLoc=FIS; isOutput=1>
               output_scalar_<name> = output_scalar_<name> + <name>;
</tracers>
             end
             if k==1
<tracers vertLoc=SUR; isOutput=1>
               output_scalar_<name> = output_scalar_<name> + <name>;
</tracers>
             end
             
             %------------------------------------
             % STEP 7.2: calculate "late" auxiliaries
             %------------------------------------
<auxiliaries vertLoc=WAT; calcAfterProcesses=1; isZGradient=0>
             % <description> :
             temp1  = <temp1>;
             temp2  = <temp2>;
             temp3  = <temp3>;
             temp4  = <temp4>;
             temp5  = <temp5>;
             temp6  = <temp6>;
             temp7  = <temp7>;
             temp8  = <temp8>;
             temp9  = <temp9>;
             <name> = <formula>;
             
</auxiliaries>

             if k == kmax
<auxiliaries vertLoc=SED; calcAfterProcesses=1; isZIntegral=0>
                % <description> :
                temp1  = <temp1>;
                temp2  = <temp2>;
                temp3  = <temp3>;
                temp4  = <temp4>;
                temp5  = <temp5>;
                temp6  = <temp6>;
                temp7  = <temp7>;
                temp8  = <temp8>;
                temp9  = <temp9>;
                <name> = <formula>;
                
</auxiliaries>
             end
             
             if k == 1
<auxiliaries vertLoc=SUR; calcAfterProcesses=1>
                % <description> :
                temp1  = <temp1>;
                temp2  = <temp2>;
                temp3  = <temp3>;
                temp4  = <temp4>;
                temp5  = <temp5>;
                temp6  = <temp6>;
                temp7  = <temp7>;
                temp8  = <temp8>;
                temp9  = <temp9>;
                <name> = <formula>;
                
</auxiliaries>
             end

             %------------------------------------
             % STEP 7.3: add values from this k level to auxiliary variables with isZIntegral=1
             %------------------------------------
<auxiliaries vertLoc=SED; calcAfterProcesses=1; isZIntegral=1>
             <name> = <name> + (<formula>)*cgt_cellheight*cgt_density;
</auxiliaries>
             
             %------------------------------------
             % STEP 7.4: output of "late" auxiliaries
             %------------------------------------
<auxiliaries vertLoc=WAT; calcAfterProcesses=1; isOutput=1>             
             output_vector_<name>(k) = output_vector_<name>(k) + <name>;
</auxiliaries>
<auxiliaries vertLoc=WAT; calcAfterProcesses=1; isUsedElsewhere=1>             
             auxiliary_vector_<name>(k) = <name>;
</auxiliaries>
             if k == kmax
<auxiliaries vertLoc=SED; calcAfterProcesses=1; isOutput=1>
                output_scalar_<name> = output_scalar_<name> + <name>;
</auxiliaries>
<auxiliaries vertLoc=WAT; calcAfterProcesses=1; isUsedElsewhere=1>             
             auxiliary_scalar_<name> = <name>;
</auxiliaries>
             end
             if k == 1
<auxiliaries vertLoc=SUR; calcAfterProcesses=1; isOutput=1>
                output_scalar_<name> = output_scalar_<name> + <name>;
</auxiliaries>
<auxiliaries vertLoc=SUR; calcAfterProcesses=1; isUsedElsewhere=1>             
             auxiliary_scalar_<name> = <name>;
</auxiliaries>
             end

             %---------------------------------------
             % STEP 7.5: passing vertical velocity and diffusivity to the coupler
             %---------------------------------------

             % EXPLICIT MOVEMENT
             <tracers vertLoc=WAT; vertSpeed/=0>
                vertical_speed_of_<name>(k)=(<vertSpeed>)/(24*3600.0); % convert to m/s
                vertical_diffusivity_of_<name>(k)=(<vertDiff>);        % leave as m2/s
             </tracers> 
    end
    
    %---------------------------------------
    % biological timestep has ended
    %---------------------------------------
    
    %---------------------------------------
    % vertical movement follows
    %---------------------------------------
    
    % calculate new total marked element concentrations
    for k = 1:kmax
          <celements>
             tracer_vector_<total>(k) = ...
             <containingTracers vertLoc=WAT>
                max(0.0,tracer_vector_<ct>(k))*<ctAmount> + ...
             </containingTracers>
                0.0;
          </celements>
    end
    % vertical movement of tracers
    for m = 1:num_vmove_steps
       % store the old age concentrations
          <celements isAging/=0>
             old_vector_<aged> = tracer_vector_<aged>;
          </celements>
       % first, move the age concentration of marked elements
       <tracers childOf/=none; vertLoc=WAT; vertSpeed/=0; hasCeAged>
          tracer_vector_<ceAgedName> = vmove_explicit(vertical_speed_of_<name>, ...
                         tracer_vector_<ceAgedName>, old_vector_<ceAgedName>, ...
                         tracer_vector_<name>*<ceAmount>, tracer_vector_<ceTotalName>, ...
                         cellheights, timestep/num_vmove_steps*(24*3600));
       </tracers>
       % second, move the tracers (including marked tracers) themselves
       <tracers vertLoc=WAT; vertSpeed/=0>
          tracer_vector_<name> = vmove_explicit(vertical_speed_of_<name>, ...
                                   tracer_vector_<name>, tracer_vector_<name>, ...
                                   tracer_vector_<name>, tracer_vector_<name>, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
       </tracers>
       % third, calculate new total marked element concentrations
       for k = 1:kmax
             <celements>
                tracer_vector_<total>(k) = ...
                <containingTracers vertLoc=WAT>
                   max(0.0,tracer_vector_<ct>(k))*<ctAmount> + ...
                </containingTracers>
                   0.0;
             </celements>       
       end
    end
   % vertical diffusion of tracers
    for m = 1:num_vmove_steps
       % store the old age concentrations
          <celements isAging/=0>
             old_vector_<aged> = tracer_vector_<aged>;
          </celements>
       % first, diffuse the age concentration of marked elements
       <tracers childOf/=none; vertLoc=WAT; vertSpeed/=0; hasCeAged>
          tracer_vector_<ceAgedName> = vdiff_explicit(vertical_diffusivity_of_<name>, ...
                                   tracer_vector_<ceAgedName>, old_vector_<ceAgedName>, ...
                                   tracer_vector_<name>*<ceAmount>, tracer_vector_<ceTotalName>, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));

       </tracers>
       % second, diffuse the tracers (including marked tracers) themselves
       <tracers vertLoc=WAT; vertSpeed/=0>
          tracer_vector_<name> = vdiff_explicit(vertical_diffusivity_of_<name>, ...
                                   tracer_vector_<name>, tracer_vector_<name>, ...
                                   tracer_vector_<name>, tracer_vector_<name>, ...
                                   cellheights, timestep/num_vmove_steps*(24*3600));
       </tracers>
       % third, calculate new total marked element concentrations
       for k = 1:kmax
             <celements>
                tracer_vector_<total>(k) = ...
                <containingTracers vertLoc=WAT>
                  max(0.0,tracer_vector_<ct>(k))*<ctAmount> + ...
                </containingTracers>
                   0.0;
             </celements>       
       end
    end

    % calculate total colored element concentrations at bottom
    <celements>
       tracer_scalar_<totalBottom> = ...
       <containingTracers vertLoc=SED>
          max(0.0,tracer_scalar_<name>)*<ctAmount> + ...
       </containingTracers>
          0.0;
    </celements>
