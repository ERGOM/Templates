%--------------------------------------------------------------------------
% save tracers, auxiliary variables and process rates to final output array
%--------------------------------------------------------------------------
% in the water column
for k=1:kmax
  <tracers vertLoc=WAT; isOutput=1>
    output_<name>(k,current_output_index)=output_vector_<name>(k)/output_count;
  </tracers>
  <auxiliaries vertLoc=WAT; isOutput=1>
    output_<name>(k,current_output_index)=output_vector_<name>(k)/output_count;
  </auxiliaries>
  <processes vertLoc=WAT; isOutput=1>
    output_<name>(k,current_output_index)=output_vector_<name>(k)/output_count;
  </processes>
end
% in the sediment
<tracers vertLoc=SED; isOutput=1>
  output_<name>(1,current_output_index)=output_scalar_<name>/output_count;
</tracers>
<auxiliaries vertLoc=SED; isOutput=1>
  output_<name>(1,current_output_index)=output_scalar_<name>/output_count;
</auxiliaries>
<processes vertLoc=SED; isOutput=1>
  output_<name>(1,current_output_index)=output_scalar_<name>/output_count;
</processes>
% at the sea surface
<tracers vertLoc=SUR; isOutput=1>
  output_<name>(1,current_output_index)=output_scalar_<name>/output_count;
</tracers>
<auxiliaries vertLoc=SUR; isOutput=1>
  output_<name>(1,current_output_index)=output_scalar_<name>/output_count;
</auxiliaries>
<processes vertLoc=SUR; isOutput=1>
  output_<name>(1,current_output_index)=output_scalar_<name>/output_count;
</processes>
% pseudo-3d tracers
<tracers vertLoc=FIS; isOutput=1>
  output_<name>(1,current_output_index)=output_scalar_<name>/output_count;
</tracers>


% now, set the temporary output vector to zero
<tracers vertLoc=WAT; isOutput=1>
  output_vector_<name> = zeros(1,kmax);
</tracers>
<auxiliaries vertLoc=WAT; isOutput=1>
  output_vector_<name> = zeros(1,kmax);
</auxiliaries>
<processes vertLoc=WAT; isOutput=1>
  output_vector_<name> = zeros(1,kmax);
</processes>
<tracers vertLoc=SED; isOutput=1>
  output_scalar_<name> = 0.0;        
</tracers>
<auxiliaries vertLoc=SED; isOutput=1>
  output_scalar_<name> = 0.0;        
</auxiliaries>
<processes vertLoc=SED; isOutput=1>
  output_scalar_<name> = 0.0;        
</processes>
<tracers vertLoc=SUR; isOutput=1>
  output_scalar_<name> = 0.0;        
</tracers>
<auxiliaries vertLoc=SUR; isOutput=1>
  output_scalar_<name> = 0.0;        
</auxiliaries>
<processes vertLoc=SUR; isOutput=1>
  output_scalar_<name> = 0.0;        
</processes>
<tracers vertLoc=FIS; isOutput=1>
  output_scalar_<name> = 0.0;        
</tracers>