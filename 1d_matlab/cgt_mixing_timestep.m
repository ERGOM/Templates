    % vertical diffusion of all tracers
    forcing_vector_diffusivity = max(forcing_vector_diffusivity,ones(size(forcing_vector_diffusivity))*min_diffusivity);
    forcing_vector_diffusivity = min(forcing_vector_diffusivity,ones(size(forcing_vector_diffusivity))*max_diffusivity);
    for m = 1:num_vdiff_steps
       % second, diffuse the tracers (including marked tracers) themselves
       <tracers vertLoc=WAT>
          tracer_vector_<name> = vdiff_explicit(forcing_vector_diffusivity, ...
                                   tracer_vector_<name>, tracer_vector_<name>, ...
                                   tracer_vector_<name>, tracer_vector_<name>, ...
                                   cellheights, timestep/num_vdiff_steps*(24*3600));
       </tracers>
    end