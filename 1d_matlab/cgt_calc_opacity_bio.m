%-----------------------------------------------------------------
% fill the vector opacity_bio with the opacity caused by POM [1/m]
%-----------------------------------------------------------------
forcing_vector_opacity_bio = zeros(1,kmax);

% water column tracers
% calculate opacity contribution [1/m] as product of
% opacity [m2/mol] * concentration [mol/kg] * water density [kg/m3]
for k=1:kmax
  <tracers vertLoc=WAT; opacity/=0>
    forcing_vector_opacity_bio(k) = forcing_vector_opacity_bio(k) + ...
        <opacity> * tracer_vector_<name>(k) * density_water;
  </tracers>
end

% surface tracers (only in uppermost cell)
% calculate opacity contribution [1/m] as product of
% opacity [m2/mol] * concentration [mol/m2] / cell height [m]
<tracers vertLoc=SUR; opacity/=0>
    forcing_vector_opacity_bio(1) = forcing_vector_opacity_bio(1) + ...
        <opacity> * tracer_scalar_<name> / cellheights(1);
</tracers>