The following initial values have to be stored in the 
file ocean_ergom.res.nc in the variable tracer_2d_1 
(and subsequently tracer_2d_2, ...) 
in layer 1, 2, ..., 20  (tracers_2d_1), 
         1, 2, ..., 20  (tracers_2d_2), ...

<tracers isFlat=1>
   layer 1 of <name>
   layer 2 of <name>
</tracers>