unit cgt_1d_model;

  //----------------------------------------------
  // PASCAL 1-d model for testing ecosystem models
  // hagen.radtke@io-warnemuende.de
  //----------------------------------------------

interface

uses NetCDFAnsi, math, SysUtils, DateUtils, Forms, sun_pos, Dialogs;


var
  time_axis: DoubleArray1d;
  depths:    DoubleArray1d;
  cellheights: DoubleArray1d;
  kmax: Integer;
  max_output_index: Integer;
  current_date: Real;

  output_temperature   : DoubleArray2d;
  output_salinity      : DoubleArray2d;
  output_opacity       : DoubleArray2d;
  output_light         : DoubleArray2d;
  output_diffusivity   : DoubleArray2d;
  output_light_at_top  : DoubleArray1d;
  output_zenith_angle  : DoubleArray1d;
  output_bottom_stress : DoubleArray1d;

  forcing_vector_temperature: DoubleArray1d;
  forcing_vector_salinity: DoubleArray1d;
  forcing_vector_opacity_water: DoubleArray1d;
  forcing_vector_opacity_bio: DoubleArray1d;
  forcing_vector_opacity: DoubleArray1d;
  forcing_vector_diffusivity: DoubleArray1d;
  forcing_vector_light: DoubleArray1d;
  forcing_scalar_light_at_top: Double;
  forcing_scalar_bottom_stress: Double;
  forcing_scalar_zenith_angle: Double;

  output_vector_temperature: DoubleArray1d;
  output_vector_salinity: DoubleArray1d;
  output_vector_opacity: DoubleArray1d;
  output_vector_light: DoubleArray1d;
  output_vector_diffusivity: DoubleArray1d;
  output_scalar_light_at_top  :Double;
  output_scalar_zenith_angle  :Double;
  output_scalar_bottom_stress :Double;


  start_date         :Real; // initial date
  end_date           :Real; // final date
  repeated_runs      :Integer;                   // how often the same forcing period is repeated
  timestep           :Real;              // timestep [days]
  timestep_increment :Real;             // increment the timestep at every timestep by a constant factor
  output_interval    :Real;             // output interval [days]
  location: TLocation; //longitude, latitude, altitude
  density_water      :Real;              // Density of water [kg/m3] to convert between mol/kg and mol/m3
  num_vmove_steps    :Integer;                   // if >1, this splits the vertical movement timestep (keep CFL criterion valid if tracers move very fast)
  num_vdiff_steps    :Integer;                  // if >1, this splits the vertical mixing (keep CFL criterion valid if tracers move very fast)
  min_diffusivity    :Real;                // minimum vertical turbulent diffusivity [m2/s]
  max_diffusivity    :Real;                   // maximum vertical turbulent diffusivity [m2/s]

procedure run(RungeKuttaDepth: Integer=1);

implementation

uses Unit1;

const
  <constants>
    <name> = <value>; // <description>
  </constants>

var
  <tracers vertLoc=WAT>
    tracer_vector_<name> : DoubleArray1d;
    tracer_vector_intermediate_<name> : DoubleArray1d;
    patankar_modification_<name> : DoubleArray1d;
  </tracers>
  <celements isAging/=0>
    old_vector_<aged> : DoubleArray1d;
  </celements>
  <tracers vertLoc/=WAT>
    tracer_scalar_<name> : Double;
    tracer_scalar_intermediate_<name> : Double;
    patankar_modification_<name> : Double;
  </tracers>

  <tracers vertLoc=WAT; vertSpeed/=0>
    vertical_speed_of_<name> : DoubleArray1d;
    vertical_diffusivity_of_<name> : DoubleArray1d;
  </tracers>
  <auxiliaries vertLoc=WAT; isUsedElsewhere=1>
    auxiliary_vector_<name>: DoubleArray1d;
  </auxiliaries>
  <auxiliaries vertLoc/=WAT; isUsedElsewhere=1>
    auxiliary_scalar_<name>: Double;
  </auxiliaries>
    
  <auxiliaries vertLoc=WAT; isOutput=1>
    output_<name> : DoubleArray2d;
    output_vector_<name> : DoubleArray1d;
  </auxiliaries>
  <auxiliaries vertLoc/=WAT; isOutput=1>
    output_<name> : DoubleArray1d;
    output_scalar_<name> : Double;
  </auxiliaries>
  <tracers vertLoc=WAT; isOutput=1>
    output_<name> : DoubleArray2d;
    output_vector_<name> : DoubleArray1d;
  </tracers>
  <tracers vertLoc/=WAT; isOutput=1>
    output_<name> : DoubleArray1d;
    output_scalar_<name> : Double;
  </tracers>
  <processes vertLoc=WAT; isOutput=1>
    output_<name> : DoubleArray2d;
    output_vector_<name> : DoubleArray1d;
  </processes>
  <processes vertLoc=WAT>
    saved_rate_<name> : DoubleArray1d;
  </processes>
  <processes vertLoc/=WAT; isOutput=1>
    output_<name> : DoubleArray1d;
    output_scalar_<name> : Double;
  </processes>
  <processes vertLoc/=WAT>
    saved_rate_<name> : Double;
  </processes>

  ivlev_lookup_table: Array[0..100] of Double;
  dz, dzr, dzrq, dztr: DoubleArray1d;


//------------------------------------------------------------
// FIRST PART
// not modified by code generation tool
//------------------------------------------------------------

procedure fill_ivlev_lookup_table;
var 
  i: Integer;
begin
  for i:=0 to length(ivlev_lookup_table)-1 do
    ivlev_lookup_table[i]:=1-exp(-0.1*i);
end;

function quick_ivlev(x: Double):Double;
var
  i_low: Integer;
  deltax: Double;
begin
  if x<0 then result:=0
  else if x>10 then result:=1
  else
  begin
    i_low:=trunc(10*x);
    deltax:=10*x-i_low;
    result:=deltax*ivlev_lookup_table[i_low+1]+(1-deltax)*ivlev_lookup_table[i_low];
  end;
end;

procedure fill_dz_arrays;
var i: Integer;
begin
  setLength(dz,kmax-1);
  setLength(dzr,kmax-1);
  setLength(dzrq,kmax-1);
  setLength(dztr,kmax);
  for i:=0 to kmax-2 do
  begin
    dz[i] := 0.5*(cellheights[i]+cellheights[i+1]);
    dzr[i] := 1.0/dz[i];
    dzrq[i]:=dzr[i]*dzr[i];
    dztr[i]:=1.0/cellheights[i];
  end;
  dztr[kmax-1]:=1.0/cellheights[kmax-1];
end;

function log(x: Double):Double;
begin
  result:=ln(x);
end;

function theta(x: Double):Double;
begin
  if x>0 then result:=1 else result:=0;
end;

function SpaceItem(var s: String):String;
begin
  s:=trim(s);
  if pos(' ',s)>0 then
  begin
    result:=trim(copy(s,1,pos(' ',s)-1));
    s:=trim(copy(s,pos(' ',s)+1,length(s)));
  end
  else
  begin
    result:=s;
    s:='';
  end;
end;

procedure disp(s: String);
begin
  Form1.Memo1.Lines.Add(s);
  Application.ProcessMessages;
end;

procedure loadMatlabMatrix(filename: String; var a:Double); overload;
var
  F: TextFile;
  s: String;
  buf: Array[1..256*256] of Byte;
begin
  decimalSeparator:='.';
  assignFile(F,filename);
  SetTextBuf(F,buf);
  reset(F);
  while not EOF(F) do
  begin
    readln(F,s);
    s:=trim(s);
    if copy(s,1,1)='%' then continue;
    if length(s)=0 then continue;
    a:=StrToFloat(SpaceItem(s));
    break;
  end;
  closefile(F);
end;

procedure loadMatlabMatrix(filename: String; var a:DoubleArray1d); overload;
var
  F: TextFile;
  s: String;
  i: Integer;
  buf: Array[1..256*256] of Byte;
begin
  setLength(a,0);
  decimalSeparator:='.';
  assignFile(F,filename);
  SetTextBuf(F,buf);
  reset(F);
  i:=0;
  while not EOF(F) do
  begin
    readln(F,s);
    s:=trim(s);
    if copy(s,1,1)='%' then continue;
    if length(s)=0 then continue;
    while length(s)>0 do
    begin
      if length(a)<i+1 then
        setLength(a,i+1);
      a[i]:=StrToFloat(SpaceItem(s));
      i:=i+1;
      s:=trim(s);
    end;
  end;
  closefile(F);
end;

procedure loadMatlabMatrix(filename: String; var a:DoubleArray2d); overload;
var
  F: TextFile;
  s: String;
  i, j: Integer;
  buf: Array[1..256*256] of Byte;
begin
  setLength(a,0,0);
  decimalSeparator:='.';
  assignFile(F,filename);
  SetTextBuf(F,buf);  reset(F);
  i:=0;
  while not EOF(F) do
  begin
    if length(a)<i+1 then
    begin
      setLength(a,i+1);
      setLength(a[i],length(a[0]));
    end;
    readln(F,s);
    s:=trim(s);
    if copy(s,1,1)='%' then continue;
    if length(s)=0 then continue;
    j:=0;
    while length(s)>0 do
    begin
      if length(a[0])<j+1 then
        setLength(a,i+1,j+1);
      a[i,j]:=StrToFloat(SpaceItem(s));
      j:=j+1;
      s:=trim(s);
    end;
    i:=i+1;
  end;
  closefile(F);
end;

procedure load_forcing( input_matrix: DoubleArray2d; orig_date, start_date, end_date: Double; kmax: Integer; var i_to_load: Integer; var final_vector: DoubleArray1d); overload;
var
  date, newdate1, newdate2: Double;
  i, k: Integer;
begin
  // if the same forcing is repeated several times, find the correct date
  // within the forcing period
  date:=orig_date;
  while date >= end_date do
      date := date - (end_date-start_date);
  // seek the index to load
  // check if current index is still valid
  i:=i_to_load;
  newdate1:=EncodeDateTime(round(input_matrix[i-1,0]),round(input_matrix[i-1,1]),round(input_matrix[i-1,2]),0,0,0,0)+input_matrix[i-1,3]/24.0;
  if i<length(input_matrix) then
    newdate2:=EncodeDateTime(round(input_matrix[i,0]),round(input_matrix[i,1]),round(input_matrix[i,2]),0,0,0,0)+input_matrix[i,3]/24.0
  else
    newdate2:=date;
  if (newdate1<date) and (newdate2>=date) then
      //everything is fine, the current vector is good
      i_to_load:=i
  else
  begin
      if (newdate1 > date) then //some time loop has finished and the date moved backwards behind the loaded date
          i:=1;
      while (i<length(input_matrix)) do
      begin
          newdate2:=EncodeDateTime(round(input_matrix[i,0]),round(input_matrix[i,1]),round(input_matrix[i,2]),0,0,0,0)+input_matrix[i,3]/24.0;
          if newdate2<date then
              i:=i+1
          else
          begin
              i_to_load:=i;
              i:=length(input_matrix)+1000;
          end;
      end;
      if i<=length(input_matrix)+1 then //found no good i in the middle => use the last one
          i_to_load:=length(input_matrix);
  end;
  // load the forcing
  for k:=0 to length(final_vector)-1 do
    final_vector[k]:=input_matrix[i_to_load-1,k+4];
end;

procedure load_forcing( input_matrix: DoubleArray2d; orig_date, start_date, end_date: Double; kmax: Integer; var i_to_load: Integer; var final_scalar: Double); overload;
var
  date, newdate1, newdate2: Double;
  i: Integer;
begin
  // if the same forcing is repeated several times, find the correct date
  // within the forcing period
  date:=orig_date;
  while date >= end_date do
      date := date - (end_date-start_date);
  // seek the index to load
  // check if current index is still valid
  i:=i_to_load;
  newdate1:=EncodeDateTime(round(input_matrix[i-1,0]),round(input_matrix[i-1,1]),round(input_matrix[i-1,2]),0,0,0,0)+input_matrix[i-1,3]/24.0;
  if i<length(input_matrix) then
    newdate2:=EncodeDateTime(round(input_matrix[i,0]),round(input_matrix[i,1]),round(input_matrix[i,2]),0,0,0,0)+input_matrix[i,3]/24.0
  else
    newdate2:=date;
  if (newdate1<date) and (newdate2>=date) then
      //everything is fine, the current vector is good
      i_to_load:=i
  else
  begin
      if (newdate1 > date) then //some time loop has finished and the date moved backwards behind the loaded date
          i:=1;
      while (i<length(input_matrix)) do
      begin
          newdate2:=EncodeDateTime(round(input_matrix[i,0]),round(input_matrix[i,1]),round(input_matrix[i,2]),0,0,0,0)+input_matrix[i,3]/24.0;
          if newdate2<date then
              i:=i+1
          else
          begin
              i_to_load:=i;
              i:=length(input_matrix)+1000;
          end;
      end;
      if i<=length(input_matrix)+1 then //found no good i in the middle => use the last one
          i_to_load:=length(input_matrix);
  end;
  // load the forcing
  final_scalar:=input_matrix[i_to_load-1,4];
end;

procedure vmove_explicit( const move: DoubleArray1d; var field: DoubleArray1d; const flux_field: DoubleArray1d; const numerator: DoubleArray1d; numFactor: Double; const denominator, dzt: DoubleArray1d; dt: Double);
var
  k: Integer;
  ft1, ft2, velocity, wpos, wneg: Double;
begin
    ft1  := 0.0;                    // upward transport through upper boundary of the cell [mol*m/kg/s]
    for k := 1 to kmax-1 do
    begin
        velocity := 0.5*move[k-1];
        wpos     := velocity + abs(velocity);                   // velocity if upward, else 0.0   [m/s]
        wneg     := velocity - abs(velocity);                   // velocity if downward, else 0.0 [m/s]
        if numerator[k-1] = denominator[k-1] then
           ft2 := (wneg*max(flux_field[k-1],0.0)+wpos*max(flux_field[k],0.0) )   
                                                               // upward transport through lower boundary of the t cell [mol*m/kg/s]
        else
           ft2 := (wneg*max(flux_field[k-1],0.0)*max(numerator[k-1]*numFactor,0.0)/max(denominator[k-1],1e-20) 
                 +wpos*max(flux_field[k],0.0)*max(numerator[k]*numFactor,0.0)/max(denominator[k],1e-20) );  
                                                               // upward transport through lower boundary of the t cell [mol*m/kg/s]
        field[k-1] := field[k-1] - dt*(ft1-ft2)/dzt[k-1];  // change in the cell due to transports through lower and upper boundary [mol/kg]
        ft1 := ft2;
    end;
    k := kmax;
    field[k-1] := field[k-1] - dt*ft1/dzt[k-1];
end;

procedure vdiff_explicit( const diff: DoubleArray1d; var field: DoubleArray1d; const flux_field: DoubleArray1d; const numerator: DoubleArray1d; numFactor: Double; const denominator, dzt: DoubleArray1d; dt: Double); overload;
var
  k: Integer;
  ft1, ft2, diffusivity, speed, mixed_height, actual_mixed_height, actual_speed: Double;
begin
    ft1  := 0.0;                    // upward transport through upper boundary of the cell [mol*m/kg/s]
    for k := 1 to kmax-1 do
    begin
        diffusivity := 0.5*diff[k-1]+0.5*diff[k];
        speed := diffusivity/(0.5*(dzt[k-1]+dzt[k])); // speed of exchange [m/s]
        mixed_height := speed*dt;                     // height of mixed water column which would be mixed if the gradient would remain the same
        // now limit it by 1/4 of the cell
        actual_mixed_height := (0.5*(dzt[k-1]+dzt[k]))*0.25*quick_ivlev(mixed_height/ (0.5*(dzt[k-1]+dzt[k]))); 
        actual_speed := actual_mixed_height/dt;
        
        ft2 := (max(flux_field[k],0.0)*max(numerator[k]*numFactor,0.0)/max(denominator[k],1e-20) 
                 -max(flux_field[k-1],0.0)*max(numerator[k-1]*numFactor,0.0)/max(denominator[k-1],1e-20)) 
                      *actual_speed; 
                                    // upward transport through lower boundary of the cell [mol*m/kg/s]
        field[k-1] := field[k-1] - dt*(ft1-ft2)/dzt[k-1];  // change in the cell due to transports through lower and upper boundary [mol/kg]
        ft1 := ft2;
    end;
    k := kmax;
    field[k-1] := field[k-1] - dt*ft1/dzt[k-1];
end;

procedure vdiff_explicit( const diff: DoubleArray1d; var field: DoubleArray1d; const dzt: DoubleArray1d; dt: Double); overload;
var
  k: Integer;
  ft1, ft2, diffusivity, mixed_height, speed, actual_mixed_height, actual_speed: Double;
begin
    ft1  := 0.0;                    // upward transport through upper boundary of the cell [mol*m/kg/s]
    for k := 1 to kmax-1 do
    begin
        diffusivity := 0.5*diff[k-1]+0.5*diff[k];
        mixed_height := dt*diffusivity*dzrq[k-1];
        mixed_height := 0.25*dz[k-1]*quick_ivlev(mixed_height);
        ft2 := (field[k]-field[k-1])*mixed_height;

        field[k-1] := field[k-1] - (ft1-ft2)*dztr[k-1];  // change in the cell due to transports through lower and upper boundary [mol/kg]
        ft1 := ft2;
    end;
    k := kmax;
    field[k-1] := field[k-1] - ft1*dztr[k-1];
end;

//------------------------------------------------------------
// SECOND PART
// modified by code generation tool
//------------------------------------------------------------

procedure cgt_init_tracers;
var 
  i: Integer;
  tempstring: String;
begin

//--------------------------------
// load initial values for tracers
//--------------------------------

// some need to be loaded from files
<tracers vertLoc=WAT; useInitValue=0>
  tempstring := trim('<name>');
  tempstring := ExtractFilePath(application.exeName)+'init\'+tempstring+'.txt';
  setLength(tracer_vector_<name>,kmax);
  setLength(tracer_vector_intermediate_<name>,kmax);
  setLength(patankar_modification_<name>,kmax);
  loadMatlabMatrix(tempstring,tracer_vector_<name>);

</tracers>
<tracers vertLoc=SED; useInitValue=0>
  tempstring := trim('<name>');
  tempstring := ExtractFilePath(application.exeName)+'init\'+tempstring+'.txt';
  loadMatlabMatrix(tempstring,tracer_scalar_<name>);

</tracers>
<tracers vertLoc=SUR; useInitValue=0>
  tempstring := trim('<name>');
  tempstring := ExtractFilePath(application.exeName)+'init\'+tempstring+'.txt';
  loadMatlabMatrix(tempstring,tracer_scalar_<name>);

</tracers>
<tracers vertLoc=FIS; useInitValue=0>
  tempstring := trim('<name>');
  tempstring := ExtractFilePath(application.exeName)+'init\'+tempstring+'.txt';
  loadMatlabMatrix(tempstring,tracer_scalar_<name>);

</tracers>

// others are initialized as constant
<tracers vertLoc=WAT; useInitValue=1>
  setLength(tracer_vector_<name>,kmax);
  setLength(tracer_vector_intermediate_<name>,kmax); 
  setLength(patankar_modification_<name>,kmax); 
  for i:=0 to kmax-1 do
    tracer_vector_<name>[i] := <initValue>;
</tracers>
<tracers vertLoc=SED; useInitValue=1>
  tracer_scalar_<name> := <initValue>;
</tracers>
<tracers vertLoc=SUR; useInitValue=1>
  tracer_scalar_<name> := <initValue>;
</tracers>
<tracers vertLoc=FIS; useInitValue=1>
  tracer_scalar_<name> := <initValue>;
</tracers>
<celements isAging/=0>
  setLength(old_vector_<aged>,kmax);
  for i:=0 to kmax-1 do
    old_vector_<aged>[i] := 0.0;
</celements>

// some tracers have vertical movement
<tracers vertLoc=WAT; vertSpeed/=0>
  setLength(vertical_speed_of_<name>,kmax);
  setLength(vertical_diffusivity_of_<name>,kmax);
  for i:=0 to kmax-1 do
  begin
    vertical_speed_of_<name>[i] := 0;
    vertical_diffusivity_of_<name>[i] := 0;
  end;
</tracers>

// auxiliaries which communicate data from the last time step are set to 0
<auxiliaries vertLoc=WAT; isUsedElsewhere=1>
  setLength(auxiliary_vector_<name>,kmax);
  for i:=0 to kmax-1 do
    auxiliary_vector_<name>[i] := 0;
</auxiliaries>
<auxiliaries vertLoc=SED; isUsedElsewhere=1>
  auxiliary_scalar_<name> := 0.0;
</auxiliaries>
<auxiliaries vertLoc=SUR; isUsedElsewhere=1>
  auxiliary_scalar_<name> := 0.0;
</auxiliaries>
end;

procedure cgt_init_output;
var
  i, j: Integer;
begin
// auxiliary variables
<auxiliaries vertLoc=WAT; isOutput=1>
  // <description> :
    setLength(output_<name>,kmax,max_output_index);  
    for i:=0 to kmax-1 do 
      for j:=0 to max_output_index-1 do 
        output_<name>[i,j]:=0;
    setLength(output_vector_<name>,kmax);  
    for i:=0 to kmax-1 do 
      output_vector_<name>[i] := 0;
</auxiliaries>
<auxiliaries vertLoc=SED; isOutput=1>
  // <description> :
    setLength(output_<name>,max_output_index);  
    for j:=0 to max_output_index-1 do 
      output_<name>[j]:=0;
    output_scalar_<name> := 0;
</auxiliaries>
<auxiliaries vertLoc=SUR; isOutput=1>
  // <description> :
    setLength(output_<name>,max_output_index);  
    for j:=0 to max_output_index-1 do 
      output_<name>[j]:=0;
    output_scalar_<name> := 0;
</auxiliaries>

// tracers
<tracers vertLoc=WAT; isOutput=1>
  // <description> :
    setLength(output_<name>,kmax,max_output_index);  
    for i:=0 to kmax-1 do 
      for j:=0 to max_output_index-1 do 
        output_<name>[i,j]:=0;
    setLength(output_vector_<name>,kmax);  
    for i:=0 to kmax-1 do 
      output_vector_<name>[i] := 0;
</tracers>
<tracers vertLoc=SED; isOutput=1>
  // <description> :
    setLength(output_<name>,max_output_index);  
    for j:=0 to max_output_index-1 do 
      output_<name>[j]:=0;
    output_scalar_<name> := 0;
</tracers>
<tracers vertLoc=SUR; isOutput=1>
  // <description> :
    setLength(output_<name>,max_output_index);  
    for j:=0 to max_output_index-1 do 
      output_<name>[j]:=0;
    output_scalar_<name> := 0;
</tracers>
<tracers vertLoc=FIS; isOutput=1>
  // <description> :
    setLength(output_<name>,max_output_index);  
    for j:=0 to max_output_index-1 do 
      output_<name>[j]:=0;
    output_scalar_<name> := 0;
</tracers>

// processes
<processes vertLoc=WAT; isOutput=1>
  // <description> :
    setLength(output_<name>,kmax,max_output_index);  
    for i:=0 to kmax-1 do 
      for j:=0 to max_output_index-1 do 
        output_<name>[i,j]:=0;
    setLength(output_vector_<name>,kmax);  
    for i:=0 to kmax-1 do 
      output_vector_<name>[i] := 0;
</processes>
<processes vertLoc=WAT>
  setLength(saved_rate_<name>,kmax);  
</processes>
<processes vertLoc=SED; isOutput=1>
  // <description> :
    setLength(output_<name>,max_output_index);  
    for j:=0 to max_output_index-1 do 
      output_<name>[j]:=0;
    output_scalar_<name> := 0;
</processes>
<processes vertLoc=SUR; isOutput=1>
  // <description> :
    setLength(output_<name>,max_output_index);  
    for j:=0 to max_output_index-1 do 
      output_<name>[j]:=0;
    output_scalar_<name> := 0;
</processes>
end;

procedure cgt_calc_opacity_bio(var forcing_vector_opacity_bio: DoubleArray1d);
var i: Integer;
begin
  for i:=0 to kmax-1 do
    forcing_vector_opacity_bio[i]:=0.0;
  // water column tracers
  // calculate opacity contribution [1/m] as product of
  // opacity [m2/mol] * concentration [mol/kg] * water density [kg/m3]
  for i:=0 to kmax-1 do
  begin
    <tracers vertLoc=WAT; opacity/=0>
      forcing_vector_opacity_bio[i] := forcing_vector_opacity_bio[i] + 
          <opacity> * tracer_vector_<name>[i] * density_water;
    </tracers>
  end;
  
  // surface tracers (only in uppermost cell)
  // calculate opacity contribution [1/m] as product of
  // opacity [m2/mol] * concentration [mol/m2] / cell height [m]
  <tracers vertLoc=SUR; opacity/=0>
      forcing_vector_opacity_bio[0] := forcing_vector_opacity_bio[0] + 
          <opacity> * tracer_scalar_<name> / cellheights[0];
  </tracers>
end;

procedure PositiveEulerTimestep(
             // input parameters
             <tracers>
                                        <name> : Double;
             </tracers>
             <auxiliaries>
                                        <name>: Double;
             </auxiliaries>
                                        cgt_temp                : Double;
                                        cgt_sali                : Double;
                                        cgt_light               : Double;
                                        cgt_cellheight          : Double;
                                        cgt_density             : Double;
                                        cgt_longitude           : Double;
                                        cgt_latitude            : Double;
                                        cgt_current_wave_stress : Double;
                                        cgt_bottomdepth         : Double;
                                        cgt_year                : Double;
                                        cgt_dayofyear           : Double;
                                        cgt_hour                : Double;
                                        cgt_iteration           : Double;
                                        k, kmax                 : Integer;
             //input/output parameters
                                    var cgt_timestep   : Double;
             <tracers>
               <limitations>
                                    var <name>: Double;
               </limitations>
             </tracers>
             //output parameters
             <processes>
                                    var total_rate_<name>: Double;
             </processes>
             <tracers vertLoc=WAT>
                                    var tracer_vector_<name>: Double;
             </tracers>
             <tracers vertLoc/=WAT>
                                    var tracer_scalar_<name>: Double;
             </tracers>
             <processes vertLoc=WAT; isOutput=1>
                                    var output_vector_<name>: Double;
             </processes>
             <processes vertLoc/=WAT; isOutput=1>
                                    var output_scalar_<name>: Double;
             </processes>
                                    var number_of_loop: Integer
                                        );
var
  timestep_fraction         : Double;
  timestep_fraction_new     : Double;
  fraction_of_total_timestep: Double;
  which_tracer_exhausted    : Integer;
<tracers>
  change_of_<name>: Double;
</tracers>
<processes>
  <name>: Double;  //<description>
</processes>
begin
             //------------------------------------
             //-- POSITIVE-DEFINITE SCHEME --------
             //-- means the following steps will be repeated as often as nessecary
             //------------------------------------
             number_of_loop:=1;
             fraction_of_total_timestep:=1.0;
             while cgt_timestep > 0.0 do
             begin

                //------------------------------------
                // STEP 1: calculate process rates
                //------------------------------------
<processes vertLoc=WAT>
                // <description> :
                <name> := <turnover>;
                <name> := max(<name>,0.0);

</processes>

                if k = kmax then
                begin
<processes vertLoc=SED>
                   // <description> :
                   <name> := <turnover>;
                   <name> := max(<name>,0.0);
                
</processes>
                end;
             
                if k = 1 then
                begin
<processes vertLoc=SUR>
                   // <description> :
                   <name> := <turnover>;
                   <name> := max(<name>,0.0);
                
</processes>
                end;

                //------------------------------------
                // STEP 2: calculate possible euler-forward change (in a full timestep)
                //------------------------------------

<tracers>
                change_of_<name> := 0.0;
</tracers>

<tracers vertLoc=WAT; hasRatesFlat=0>
             
                change_of_<name> := change_of_<name> + cgt_timestep*(0.0 
                <timeTendencies vertLoc=WAT>
                   <timeTendency>  // <description>
                </timeTendencies>
                );
</tracers>
<tracers vertLoc=FIS; hasRatesFlat=0>
             
                change_of_<name> := change_of_<name> + cgt_timestep*(0.0 
                <timeTendencies vertLoc=WAT>
                   <timeTendency>  // <description>
                </timeTendencies>
                );
</tracers>


                if k = 1 then
                begin
<tracers vertLoc=WAT; hasRatesFlat=2>

                   change_of_<name> := change_of_<name> + cgt_timestep*(0.0 
                   <timeTendencies vertLoc=SUR>
                      <timeTendency>  // <description>
                   </timeTendencies>
                   );
</tracers>
                end;

                if k = kmax then
                begin
<tracers vertLoc=WAT; hasRatesFlat=1>

                   change_of_<name> := change_of_<name> + cgt_timestep*(0.0 
                   <timeTendencies vertLoc=SED>
                      <timeTendency>  // <description>
                   </timeTendencies>
                   );
</tracers>
<tracers vertLoc=SED; hasRates>

                   change_of_<name> := change_of_<name> + cgt_timestep*(0.0 
                   <timeTendencies>
                      <timeTendency>  // <description>
                   </timeTendencies>
                   );
</tracers>                         
<tracers vertLoc=FIS; hasRatesFlat=1>

                   change_of_<name> := change_of_<name> + cgt_timestep*(0.0 
                   <timeTendencies vertLoc=SED>
                      <timeTendency>  // <description>
                   </timeTendencies>
                   );
</tracers>

                end;           

                //------------------------------------
                // STEP 3: calculate maximum fraction of the timestep before some tracer gets exhausted
                //------------------------------------

                timestep_fraction := 1.0;
                which_tracer_exhausted := -1;

                // find the tracer which is exhausted after the shortest period of time

                // in the water column
<tracers vertLoc=WAT; isPositive=1>
             
                // check if tracer <name> was exhausted from the beginning and is still consumed
                if (tracer_vector_<name> <= 0.0) and (change_of_<name> < 0.0) then
                begin
                   timestep_fraction := 0.0;
                   which_tracer_exhausted := <numTracer>;
                end;
                // check if tracer <name> was present, but got exhausted
                if (tracer_vector_<name> > 0.0) and (tracer_vector_<name> + change_of_<name> < 0.0) then
                begin
                   timestep_fraction_new := tracer_vector_<name> / (0.0 - change_of_<name>);
                   if timestep_fraction_new <= timestep_fraction then
                   begin
                      which_tracer_exhausted := <numTracer>;
                      timestep_fraction := timestep_fraction_new;
                   end;
                end;
</tracers>
          
                // in the bottom layer
                if k = kmax then
                begin
<tracers vertLoc=SED; isPositive=1>

                   // check if tracer <name> was exhausted from the beginning and is still consumed
                   if (tracer_scalar_<name> <= 0.0) and (change_of_<name> < 0.0) then
                   begin
                      timestep_fraction := 0.0;
                      which_tracer_exhausted := <numTracer>;
                   end;
                   // check if tracer <name> was present, but got exhausted
                   if (tracer_scalar_<name> > 0.0) and (tracer_scalar_<name> + change_of_<name> < 0.0) then
                   begin
                      timestep_fraction_new := tracer_scalar_<name> / (0.0 - change_of_<name>);
                      if timestep_fraction_new <= timestep_fraction then
                      begin
                         which_tracer_exhausted := <numTracer>;
                         timestep_fraction := timestep_fraction_new;
                      end;
                   end;
</tracers>                         
                end;          

                // now, update the limitations: rates of the processes limited by this tracer become zero in the future

<tracers isPositive=1; vertLoc/=FIS>
                if <numTracer> = which_tracer_exhausted then
                begin
                  <limitations>
                   <name> := 0.0;
                  </limitations>
                end;
</tracers>

                //------------------------------------
                // STEP 4: apply a Euler-forward timestep with the fraction of the time
                //------------------------------------ 

                // in the water column
<tracers vertLoc=WAT>
             
                // tracer <name> (<description>):
                tracer_vector_<name> := tracer_vector_<name> + change_of_<name> * timestep_fraction;
</tracers>
<tracers vertLoc=FIS>
             
                // tracer <name> (<description>):
                cumulated_change_of_<name> := cumulated_change_of_<name> + change_of_<name> * timestep_fraction;
</tracers>

          
                // in the bottom layer
                if k = kmax then
                begin
<tracers vertLoc=SED>

                   // tracer <name> (<description>)
                   tracer_scalar_<name> := tracer_scalar_<name> + change_of_<name> * timestep_fraction;
</tracers>                         
                end;

                //------------------------------------
                // STEP 5: output of process rates
                //------------------------------------
<processes vertLoc=WAT; isOutput=1>             
                output_vector_<name> := output_vector_<name> + <name> * timestep_fraction * fraction_of_total_timestep;
</processes>
                if k = kmax then
                begin
<processes vertLoc=SED; isOutput=1>
                   output_scalar_<name> := output_scalar_<name> + <name> * timestep_fraction * fraction_of_total_timestep;
</processes>
                end;
                if k = 1 then
                begin
<processes vertLoc=SUR; isOutput=1>
                   output_scalar_<name> := output_scalar_<name> + <name> * timestep_fraction * fraction_of_total_timestep;
</processes>
                end;
             
                //------------------------------------
                // STEP 6: set timestep to remaining timestep only
                //------------------------------------

                cgt_timestep := cgt_timestep * (1.0 - timestep_fraction);                         // remaining timestep
                fraction_of_total_timestep := fraction_of_total_timestep * (1.0 - timestep_fraction); // how much of the original timestep is remaining


                if number_of_loop > 100 then
                begin
                  ShowMessage('aborted positive-definite scheme: more than 100 iterations');
                end;
                number_of_loop:=number_of_loop+1;

             end;
             //------------------------------------
             //-- END OF POSITIVE-DEFINITE SCHEME -
             //------------------------------------  
end;

procedure PatankarTimestep(
             // input parameters
             <tracers vertLoc=WAT>
                                        <name> : Double;
                                        patankar_modification_<name>: Double;
             </tracers>
             <tracers vertLoc/=WAT>
                                        <name> : Double;
                                        patankar_modification_<name>: Double;
             </tracers>
             <auxiliaries>
                                        <name>: Double;
             </auxiliaries>
             <processes vertLoc=WAT>
                                        saved_rate_<name>: Double;
             </processes>
             <processes vertLoc/=WAT>
                                        saved_rate_<name>: Double;
             </processes>
                                        cgt_temp                : Double;
                                        cgt_sali                : Double;
                                        cgt_light               : Double;
                                        cgt_cellheight          : Double;
                                        cgt_density             : Double;
                                        cgt_longitude           : Double;
                                        cgt_latitude            : Double;
                                        cgt_current_wave_stress : Double;
                                        cgt_bottomdepth         : Double;
                                        cgt_year                : Double;
                                        cgt_dayofyear           : Double;
                                        cgt_hour                : Double;
                                        cgt_iteration           : Double;
                                        k, kmax                 : Integer;
             //input/output parameters
                                    var cgt_timestep   : Double;
             <tracers>
               <limitations>
                                    var <name>: Double;
               </limitations>
             </tracers>
             //output parameters
             <processes>
                                    var total_rate_<name>: Double;
             </processes>
             <tracers vertLoc=WAT>
                                    var tracer_vector_<name>: Double;
             </tracers>
             <tracers vertLoc/=WAT>
                                    var tracer_scalar_<name>: Double;
             </tracers>
             <processes vertLoc=WAT; isOutput=1>
                                    var output_vector_<name>: Double;
             </processes>
             <processes vertLoc/=WAT; isOutput=1>
                                    var output_scalar_<name>: Double;
             </processes>
                                    var number_of_loop: Integer
                                        );
var
  timestep_fraction         : Double;
  timestep_fraction_new     : Double;
  fraction_of_total_timestep: Double;
  which_tracer_exhausted    : Integer;
<tracers>
  change_of_<name>: Double;
</tracers>
<processes>
  <name>: Double;  //<description>
</processes>
<processes isStiff/=0>
  after_patankar_<name>: Double;
</processes>
begin
             //------------------------------------
             //-- POSITIVE-DEFINITE SCHEME --------
             //-- means the following steps will be repeated as often as nessecary
             //------------------------------------
             number_of_loop:=1;
             fraction_of_total_timestep:=1.0;
             while cgt_timestep > 0.0 do
             begin

                //------------------------------------
                // STEP 1: calculate process rates
                //------------------------------------
<processes vertLoc=WAT>
                // <description> :
                <name> := <turnover>;
                <name> := max(<name>,0.0);

</processes>

                if k = kmax then
                begin
<processes vertLoc=SED>
                   // <description> :
                   <name> := <turnover>;
                   <name> := max(<name>,0.0);
                
</processes>
                end;
             
                if k = 1 then
                begin
<processes vertLoc=SUR>
                   // <description> :
                   <name> := <turnover>;
                   <name> := max(<name>,0.0);
                
</processes>
                end;

                //------------------------------------
                // STEP 1.0.1: use saved rates if they apply
                //------------------------------------

<processes vertLoc=WAT>
                if (<name> > 0) and (saved_rate_<name> >= 0) then <name>:=saved_rate_<name>;
</processes>
<processes vertLoc=WAT; isStiff/=0>
                if (<name> > 0) and (saved_rate_<name> >= 0) then <name>:=<name> * patankar_modification_<stiffTracer>;
</processes>

                if k = kmax then
                begin
<processes vertLoc=SED>
                  if (<name> > 0) and (saved_rate_<name> >= 0) then <name>:=saved_rate_<name>;
</processes>
<processes vertLoc=SED; isStiff/=0>
                  if (<name> > 0) and (saved_rate_<name> >= 0) then <name>:=<name> * patankar_modification_<stiffTracer>;
</processes>
                end;
             
                if k = 1 then
                begin
<processes vertLoc=SUR>
                  if (<name> > 0) and (saved_rate_<name> >= 0) then <name>:=saved_rate_<name>;
</processes>
<processes vertLoc=SUR; isStiff/=0>
                  if (<name> > 0) and (saved_rate_<name> >= 0) then <name>:=<name> * patankar_modification_<stiffTracer>;
</processes>
                end;
                
                //------------------------------------
                // STEP 1.1: apply Patankar limitations
                //------------------------------------
<processes vertLoc=WAT; isStiff/=0>
                // <description> :
                after_patankar_<name> := <name> * <stiffFactor>;

</processes>

                if k = kmax then
                begin
<processes vertLoc=SED; isStiff/=0>
                   // <description> :
                after_patankar_<name> := <name> * <stiffFactor>;
                
</processes>
                end;
             
                if k = 1 then
                begin
<processes vertLoc=SUR; isStiff/=0>
                   // <description> :
                after_patankar_<name> := <name> * <stiffFactor>;
                
</processes>
                end;

<processes vertLoc=WAT; isStiff/=0>
                // <description> :
                <name> := after_patankar_<name>;

</processes>

                if k = kmax then
                begin
<processes vertLoc=SED; isStiff/=0>
                   // <description> :
                <name> := after_patankar_<name>;
                
</processes>
                end;
             
                if k = 1 then
                begin
<processes vertLoc=SUR; isStiff/=0>
                   // <description> :
                <name> := after_patankar_<name>;
                
</processes>
                end;
                //------------------------------------
                // STEP 2: calculate possible euler-forward change (in a full timestep)
                //------------------------------------

<tracers>
                change_of_<name> := 0.0;
</tracers>

<tracers vertLoc=WAT; hasRatesFlat=0>
             
                change_of_<name> := change_of_<name> + cgt_timestep*(0.0 
                <timeTendencies vertLoc=WAT>
                   <timeTendency>  // <description>
                </timeTendencies>
                );
</tracers>
<tracers vertLoc=FIS; hasRatesFlat=0>
             
                change_of_<name> := change_of_<name> + cgt_timestep*(0.0 
                <timeTendencies vertLoc=WAT>
                   <timeTendency>  // <description>
                </timeTendencies>
                );
</tracers>


                if k = 1 then
                begin
<tracers vertLoc=WAT; hasRatesFlat=2>

                   change_of_<name> := change_of_<name> + cgt_timestep*(0.0 
                   <timeTendencies vertLoc=SUR>
                      <timeTendency>  // <description>
                   </timeTendencies>
                   );
</tracers>
                end;

                if k = kmax then
                begin
<tracers vertLoc=WAT; hasRatesFlat=1>

                   change_of_<name> := change_of_<name> + cgt_timestep*(0.0 
                   <timeTendencies vertLoc=SED>
                      <timeTendency>  // <description>
                   </timeTendencies>
                   );
</tracers>
<tracers vertLoc=SED; hasRates>

                   change_of_<name> := change_of_<name> + cgt_timestep*(0.0 
                   <timeTendencies>
                      <timeTendency>  // <description>
                   </timeTendencies>
                   );
</tracers>                         
<tracers vertLoc=FIS; hasRatesFlat=1>

                   change_of_<name> := change_of_<name> + cgt_timestep*(0.0 
                   <timeTendencies vertLoc=SED>
                      <timeTendency>  // <description>
                   </timeTendencies>
                   );
</tracers>

                end;           

                //------------------------------------
                // STEP 3: calculate maximum fraction of the timestep before some tracer gets exhausted
                //------------------------------------

                timestep_fraction := 1.0;
                which_tracer_exhausted := -1;

                // find the tracer which is exhausted after the shortest period of time

                // in the water column
<tracers vertLoc=WAT; isPositive=1>
             
                // check if tracer <name> was exhausted from the beginning and is still consumed
                if (tracer_vector_<name> <= 0.0) and (change_of_<name> < 0.0) then
                begin
                   timestep_fraction := 0.0;
                   which_tracer_exhausted := <numTracer>;
                end;
                // check if tracer <name> was present, but got exhausted
                if (tracer_vector_<name> > 0.0) and (tracer_vector_<name> + change_of_<name> < 0.0) then
                begin
                   timestep_fraction_new := tracer_vector_<name> / (0.0 - change_of_<name>);
                   if timestep_fraction_new <= timestep_fraction then
                   begin
                      which_tracer_exhausted := <numTracer>;
                      timestep_fraction := timestep_fraction_new;
                   end;
                end;
</tracers>
          
                // in the bottom layer
                if k = kmax then
                begin
<tracers vertLoc=SED; isPositive=1>

                   // check if tracer <name> was exhausted from the beginning and is still consumed
                   if (tracer_scalar_<name> <= 0.0) and (change_of_<name> < 0.0) then
                   begin
                      timestep_fraction := 0.0;
                      which_tracer_exhausted := <numTracer>;
                   end;
                   // check if tracer <name> was present, but got exhausted
                   if (tracer_scalar_<name> > 0.0) and (tracer_scalar_<name> + change_of_<name> < 0.0) then
                   begin
                      timestep_fraction_new := tracer_scalar_<name> / (0.0 - change_of_<name>);
                      if timestep_fraction_new <= timestep_fraction then
                      begin
                         which_tracer_exhausted := <numTracer>;
                         timestep_fraction := timestep_fraction_new;
                      end;
                   end;
</tracers>                         
                end;          

                // now, update the limitations: rates of the processes limited by this tracer become zero in the future

<tracers isPositive=1; vertLoc/=FIS>
                if <numTracer> = which_tracer_exhausted then
                begin
                  <limitations>
                   <name> := 0.0;
                  </limitations>
                end;
</tracers>

                //------------------------------------
                // STEP 4: apply a Euler-forward timestep with the fraction of the time
                //------------------------------------ 

                // in the water column
<tracers vertLoc=WAT>
             
                // tracer <name> (<description>):
                tracer_vector_<name> := tracer_vector_<name> + change_of_<name> * timestep_fraction;
</tracers>
<tracers vertLoc=FIS>
             
                // tracer <name> (<description>):
                cumulated_change_of_<name> := cumulated_change_of_<name> + change_of_<name> * timestep_fraction;
</tracers>

          
                // in the bottom layer
                if k = kmax then
                begin
<tracers vertLoc=SED>

                   // tracer <name> (<description>)
                   tracer_scalar_<name> := tracer_scalar_<name> + change_of_<name> * timestep_fraction;
</tracers>                         
                end;

                //------------------------------------
                // STEP 5: output of process rates
                //------------------------------------
<processes vertLoc=WAT; isOutput=1>             
                output_vector_<name> := output_vector_<name> + <name> * timestep_fraction * fraction_of_total_timestep;
</processes>
                if k = kmax then
                begin
<processes vertLoc=SED; isOutput=1>
                   output_scalar_<name> := output_scalar_<name> + <name> * timestep_fraction * fraction_of_total_timestep;
</processes>
                end;
                if k = 1 then
                begin
<processes vertLoc=SUR; isOutput=1>
                   output_scalar_<name> := output_scalar_<name> + <name> * timestep_fraction * fraction_of_total_timestep;
</processes>
                end;
             
                //------------------------------------
                // STEP 6: set timestep to remaining timestep only
                //------------------------------------

                cgt_timestep := cgt_timestep * (1.0 - timestep_fraction);                         // remaining timestep
                fraction_of_total_timestep := fraction_of_total_timestep * (1.0 - timestep_fraction); // how much of the original timestep is remaining


                if number_of_loop > 100 then
                begin
                  ShowMessage('aborted positive-definite scheme: more than 100 iterations');
                end;
                number_of_loop:=number_of_loop+1;

             end;
             //------------------------------------
             //-- END OF POSITIVE-DEFINITE SCHEME -
             //------------------------------------  
end;

procedure PatankarTimestepIntermediate(
             // input parameters
             <tracers>
                                        <name> : Double;
             </tracers>
             <auxiliaries>
                                        <name>: Double;
             </auxiliaries>
                                        cgt_temp                : Double;
                                        cgt_sali                : Double;
                                        cgt_light               : Double;
                                        cgt_cellheight          : Double;
                                        cgt_density             : Double;
                                        cgt_longitude           : Double;
                                        cgt_latitude            : Double;
                                        cgt_current_wave_stress : Double;
                                        cgt_bottomdepth         : Double;
                                        cgt_year                : Double;
                                        cgt_dayofyear           : Double;
                                        cgt_hour                : Double;
                                        cgt_iteration           : Double;
                                        k, kmax                 : Integer;
             //input/output parameters
                                    var cgt_timestep   : Double;
             <tracers>
               <limitations>
                                    var <name>: Double;
               </limitations>
             </tracers>
             //output parameters
             <processes>
                                    var total_rate_<name>: Double;
             </processes>
             <tracers vertLoc=WAT>
                                    var tracer_vector_<name>: Double;
             </tracers>
             <tracers vertLoc/=WAT>
                                    var tracer_scalar_<name>: Double;
             </tracers>
                                    var number_of_loop: Integer
                                        );
var
  timestep_fraction         : Double;
  timestep_fraction_new     : Double;
  fraction_of_total_timestep: Double;
  which_tracer_exhausted    : Integer;
<tracers>
  change_of_<name>: Double;
</tracers>
<processes>
  <name>: Double;  //<description>
</processes>
<processes isStiff/=0>
  after_patankar_<name>: Double;
</processes>
begin
             //------------------------------------
             //-- POSITIVE-DEFINITE SCHEME --------
             //-- means the following steps will be repeated as often as nessecary
             //------------------------------------
             number_of_loop:=1;
             fraction_of_total_timestep:=1.0;
             while cgt_timestep > 0.0 do
             begin

                //------------------------------------
                // STEP 1: calculate process rates
                //------------------------------------
<processes vertLoc=WAT>
                // <description> :
                <name> := <turnover>;
                <name> := max(<name>,0.0);

</processes>

                if k = kmax then
                begin
<processes vertLoc=SED>
                   // <description> :
                   <name> := <turnover>;
                   <name> := max(<name>,0.0);
                
</processes>
                end;
             
                if k = 1 then
                begin
<processes vertLoc=SUR>
                   // <description> :
                   <name> := <turnover>;
                   <name> := max(<name>,0.0);
                
</processes>
                end;

                //------------------------------------
                // STEP 1.0.1: save process rates
                //------------------------------------
                if number_of_loop=1 then
                begin
<processes vertLoc=WAT>
                  saved_rate_<name>[k-1]:=saved_rate_<name>[k-1]+<name>;
</processes>
<processes vertLoc=SED>
                  if k = kmax then saved_rate_<name>:=saved_rate_<name>+<name>;
</processes>
<processes vertLoc=SUR>
                  if k = 1 then saved_rate_<name>:=saved_rate_<name>+<name>;
</processes>
                end;

                //------------------------------------
                // STEP 1.1: apply Patankar limitations
                //------------------------------------
<processes vertLoc=WAT; isStiff/=0>
                // <description> :
                after_patankar_<name> := <name> * <stiffFactor>;

</processes>

                if k = kmax then
                begin
<processes vertLoc=SED; isStiff/=0>
                   // <description> :
                after_patankar_<name> := <name> * <stiffFactor>;
                
</processes>
                end;
             
                if k = 1 then
                begin
<processes vertLoc=SUR; isStiff/=0>
                   // <description> :
                after_patankar_<name> := <name> * <stiffFactor>;
                
</processes>
                end;

<processes vertLoc=WAT; isStiff/=0>
                // <description> :
                <name> := after_patankar_<name>;

</processes>

                if k = kmax then
                begin
<processes vertLoc=SED; isStiff/=0>
                   // <description> :
                <name> := after_patankar_<name>;
                
</processes>
                end;
             
                if k = 1 then
                begin
<processes vertLoc=SUR; isStiff/=0>
                   // <description> :
                <name> := after_patankar_<name>;
                
</processes>
                end;
                //------------------------------------
                // STEP 2: calculate possible euler-forward change (in a full timestep)
                //------------------------------------

<tracers>
                change_of_<name> := 0.0;
</tracers>

<tracers vertLoc=WAT; hasRatesFlat=0>
             
                change_of_<name> := change_of_<name> + cgt_timestep*(0.0 
                <timeTendencies vertLoc=WAT>
                   <timeTendency>  // <description>
                </timeTendencies>
                );
</tracers>
<tracers vertLoc=FIS; hasRatesFlat=0>
             
                change_of_<name> := change_of_<name> + cgt_timestep*(0.0 
                <timeTendencies vertLoc=WAT>
                   <timeTendency>  // <description>
                </timeTendencies>
                );
</tracers>


                if k = 1 then
                begin
<tracers vertLoc=WAT; hasRatesFlat=2>

                   change_of_<name> := change_of_<name> + cgt_timestep*(0.0 
                   <timeTendencies vertLoc=SUR>
                      <timeTendency>  // <description>
                   </timeTendencies>
                   );
</tracers>
                end;

                if k = kmax then
                begin
<tracers vertLoc=WAT; hasRatesFlat=1>

                   change_of_<name> := change_of_<name> + cgt_timestep*(0.0 
                   <timeTendencies vertLoc=SED>
                      <timeTendency>  // <description>
                   </timeTendencies>
                   );
</tracers>
<tracers vertLoc=SED; hasRates>

                   change_of_<name> := change_of_<name> + cgt_timestep*(0.0 
                   <timeTendencies>
                      <timeTendency>  // <description>
                   </timeTendencies>
                   );
</tracers>                         
<tracers vertLoc=FIS; hasRatesFlat=1>

                   change_of_<name> := change_of_<name> + cgt_timestep*(0.0 
                   <timeTendencies vertLoc=SED>
                      <timeTendency>  // <description>
                   </timeTendencies>
                   );
</tracers>

                end;           

                //------------------------------------
                // STEP 3: calculate maximum fraction of the timestep before some tracer gets exhausted
                //------------------------------------

                timestep_fraction := 1.0;
                which_tracer_exhausted := -1;

                // find the tracer which is exhausted after the shortest period of time

                // in the water column
<tracers vertLoc=WAT; isPositive=1>
             
                // check if tracer <name> was exhausted from the beginning and is still consumed
                if (tracer_vector_<name> <= 0.0) and (change_of_<name> < 0.0) then
                begin
                   timestep_fraction := 0.0;
                   which_tracer_exhausted := <numTracer>;
                end;
                // check if tracer <name> was present, but got exhausted
                if (tracer_vector_<name> > 0.0) and (tracer_vector_<name> + change_of_<name> < 0.0) then
                begin
                   timestep_fraction_new := tracer_vector_<name> / (0.0 - change_of_<name>);
                   if timestep_fraction_new <= timestep_fraction then
                   begin
                      which_tracer_exhausted := <numTracer>;
                      timestep_fraction := timestep_fraction_new;
                   end;
                end;
</tracers>
          
                // in the bottom layer
                if k = kmax then
                begin
<tracers vertLoc=SED; isPositive=1>

                   // check if tracer <name> was exhausted from the beginning and is still consumed
                   if (tracer_scalar_<name> <= 0.0) and (change_of_<name> < 0.0) then
                   begin
                      timestep_fraction := 0.0;
                      which_tracer_exhausted := <numTracer>;
                   end;
                   // check if tracer <name> was present, but got exhausted
                   if (tracer_scalar_<name> > 0.0) and (tracer_scalar_<name> + change_of_<name> < 0.0) then
                   begin
                      timestep_fraction_new := tracer_scalar_<name> / (0.0 - change_of_<name>);
                      if timestep_fraction_new <= timestep_fraction then
                      begin
                         which_tracer_exhausted := <numTracer>;
                         timestep_fraction := timestep_fraction_new;
                      end;
                   end;
</tracers>                         
                end;          

                // now, update the limitations: rates of the processes limited by this tracer become zero in the future

<tracers isPositive=1; vertLoc/=FIS>
                if <numTracer> = which_tracer_exhausted then
                begin
                  <limitations>
                   <name> := 0.0;
                  </limitations>
                end;
</tracers>

                //------------------------------------
                // STEP 4: apply a Euler-forward timestep with the fraction of the time
                //------------------------------------ 

                // in the water column
<tracers vertLoc=WAT>
             
                // tracer <name> (<description>):
                tracer_vector_<name> := tracer_vector_<name> + change_of_<name> * timestep_fraction;
</tracers>
<tracers vertLoc=FIS>
             
                // tracer <name> (<description>):
                cumulated_change_of_<name> := cumulated_change_of_<name> + change_of_<name> * timestep_fraction;
</tracers>

          
                // in the bottom layer
                if k = kmax then
                begin
<tracers vertLoc=SED>

                   // tracer <name> (<description>)
                   tracer_scalar_<name> := tracer_scalar_<name> + change_of_<name> * timestep_fraction;
</tracers>                         
                end;

                //------------------------------------
                // STEP 5: output of process rates
                //------------------------------------
             
                //------------------------------------
                // STEP 6: set timestep to remaining timestep only
                //------------------------------------

                cgt_timestep := cgt_timestep * (1.0 - timestep_fraction);                         // remaining timestep
                fraction_of_total_timestep := fraction_of_total_timestep * (1.0 - timestep_fraction); // how much of the original timestep is remaining


                if number_of_loop > 100 then
                begin
                  ShowMessage('aborted positive-definite scheme: more than 100 iterations');
                end;
                number_of_loop:=number_of_loop+1;

             end;
             //------------------------------------
             //-- END OF POSITIVE-DEFINITE SCHEME -
             //------------------------------------  
end;


procedure cgt_bio_timestep(useSavedRates: Boolean=false);
var
  k, m: Integer;

  cgt_temp                : Double;           // potential temperature     [Celsius]
  cgt_sali                : Double;           // salinity                  [g/kg]
  cgt_light               : Double;           // light intensity           [W/m2]
  cgt_cellheight          : Double;           // cell height               [m]
  cgt_density             : Double;           // density                   [kg/m3]
  cgt_timestep            : Double;           // timestep                  [days]
  cgt_longitude           : Double;           // geographic longitude      [deg]
  cgt_latitude            : Double;           // geographic latitude       [deg]
  cgt_current_wave_stress : Double;           // bottom stress             [N/m2]
  cgt_bottomdepth         : Double;           // bottom depth              [m]
  cgt_year                : Double;           // year (integer value)      [years]
  cgt_dayofyear           : Double;           // julian day of the year (integer value) [days]
  cgt_hour                : Double;           // hour plus fraction (0..23.99) [hours]
  cgt_iteration           : Integer;          // number of iteration in iterative loop [1]

  number_of_loop            : Integer;

  temp1                     : Double;
  temp2                     : Double;
  temp3                     : Double;
  temp4                     : Double;
  temp5                     : Double;
  temp6                     : Double;
  temp7                     : Double;
  temp8                     : Double;
  temp9                     : Double;

  <tracers>
    <name>       : Double; // <description>
    <limitations>
      <name> : Double;
    </limitations>
  </tracers>
  <auxiliaries>
    <name>       : Double; // <description>
  </auxiliaries>
  <processes>
    total_rate_<name> : Double;
  </processes>
  <tracers vertLoc=WAT>
    above_<name> : Double; 
  </tracers>
  <tracers vertLoc=FIS>
    cumulated_change_of_<name> : Double;
  </tracers>
begin
    // calculate total element concentrations in the water column
    for k := 1 to kmax do
    begin
          <celements>
             tracer_vector_<total>[k-1] := 
             <containingTracers vertLoc=WAT>
                max(0.0,tracer_vector_<ct>[k-1])*<ctAmount> + 
             </containingTracers>
                0.0;
          </celements>   
    end;

    // calculate total colored element concentrations at bottom
       <celements>
          tracer_scalar_<totalBottom> := 
          <containingTracers vertLoc=SED>
             max(0.0,tracer_scalar_<ct>)*<ctAmount> + 
          </containingTracers>
             0.0;
       </celements>    

        // First, do the Pre-Loop to calculate isZIntegral=1 auxiliaries

    // initialize isZIntegral auxiliary variables with zero
    <auxiliaries isZIntegral=1; calcAfterProcesses=0>
       <name> := 0.0; 
    </auxiliaries>

    cgt_year     :=yearOf(current_date);
    cgt_dayofyear:=dayOfTheYear(current_date);
    cgt_hour     :=hourOf(current_date)+minuteOf(current_date)/60+secondOf(current_date)/3600;

    cgt_bottomdepth := 0.0;
    for k := 1 to kmax do
    begin
       cgt_bottomdepth := cgt_bottomdepth + cellheights[k-1];
          
             //------------------------------------
             // STEP 0.1: prepare abiotic parameters
             //------------------------------------
             cgt_temp       := forcing_vector_temperature[k-1];         // potential temperature     [Celsius]
             cgt_sali       := forcing_vector_salinity[k-1];            // salinity                  [g/kg]
             cgt_light      := forcing_vector_light[k-1];               // light intensity           [W/m2]
             cgt_cellheight := cellheights[k-1];                        // cell height               [m]
             cgt_density    := density_water;                           // density                   [kg/m3]
             cgt_timestep   := timestep;                                // timestep                  [days]
             cgt_longitude  := location.longitude;                      // geographic longitude      [deg]
             cgt_latitude   := location.latitude;                       // geographic latitude       [deg]
             if k = kmax then
                cgt_current_wave_stress:=forcing_scalar_bottom_stress;  // bottom stress             [N/m2]
             
             //------------------------------------
             // STEP 0.2: load tracer values
             //------------------------------------
<tracers vertLoc=WAT; calcBeforeZIntegral=1>
             <name> := tracer_vector_<name>[k-1]; // <description>
             if k = 1 then
                above_<name> := tracer_vector_<name>[k-1]
             else
                above_<name> := tracer_vector_<name>[k-2];
</tracers>
<tracers vertLoc=FIS; calcBeforeZIntegral=1>
             <name> := tracer_scalar_<name>; // <description>
</tracers>             
<auxiliaries vertLoc=WAT; isUsedElsewhere=1; calcBeforeZIntegral=1>
             <name> := auxiliary_vector_<name>[k-1]; // <description>
</auxiliaries>

<tracers vertLoc=WAT; isPositive=1; calcBeforeZIntegral=1>
             <name>       := max(<name>,0.0);
             above_<name> := max(above_<name>,0.0);
</tracers>
<tracers vertLoc=FIS; isPositive=1; calcBeforeZIntegral=1>
             <name>       := max(<name>,0.0);
</tracers>

             if k = kmax then
             begin
<tracers vertLoc=SED; calcBeforeZIntegral=1>
                <name> := tracer_scalar_<name>; // <description>
</tracers>

<tracers vertLoc=SED; isPositive=1; calcBeforeZIntegral=1>
                <name> := max(<name>,0.0);
</tracers>
             end;

             //------------------------------------
             // STEP 0.3: calculate auxiliaries
             //------------------------------------

<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=1; calcBeforeZIntegral=1>
             // <description> :
             <name> := (above_<formula>-<formula>)/cgt_cellheight;

</auxiliaries>

             //initialize auxiliaries for iterative loop
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; calcBeforeZIntegral=1; iterations/=0>
             <name> := <iterInit>;
</auxiliaries>

             //iterative loop follows
             for cgt_iteration:=1 to <maxIterations> do
             begin
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; calcBeforeZIntegral=1; iterations/=0>
               // <description> :
               if cgt_iteration <= <iterations> then
               begin
                 temp1  := <temp1>;
                 temp2  := <temp2>;
                 temp3  := <temp3>;
                 temp4  := <temp4>;
                 temp5  := <temp5>;
                 temp6  := <temp6>;
                 temp7  := <temp7>;
                 temp8  := <temp8>;
                 temp9  := <temp9>;
                 <name> := <formula>;
               end;
</auxiliaries>
             end;

<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; calcBeforeZIntegral=1; iterations=0>
             // <description> :
             temp1  := <temp1>;
             temp2  := <temp2>;
             temp3  := <temp3>;
             temp4  := <temp4>;
             temp5  := <temp5>;
             temp6  := <temp6>;
             temp7  := <temp7>;
             temp8  := <temp8>;
             temp9  := <temp9>;
             <name> := <formula>;
             
</auxiliaries>

             if k = kmax then
             begin
               //initialize auxiliaries for iterative loop
<auxiliaries vertLoc=SED; calcAfterProcesses=0; isZIntegral=0; calcBeforeZIntegral=1; iterations/=0>
               <name> := <iterInit>;
</auxiliaries>

               //iterative loop follows
               for cgt_iteration:=1 to <maxIterations> do
               begin
<auxiliaries vertLoc=SED; calcAfterProcesses=0; isZIntegral=0; calcBeforeZIntegral=1; iterations/=0>
                 // <description> :
                 if cgt_iteration <= <iterations> then
                 begin
                   temp1  := <temp1>;
                   temp2  := <temp2>;
                   temp3  := <temp3>;
                   temp4  := <temp4>;
                   temp5  := <temp5>;
                   temp6  := <temp6>;
                   temp7  := <temp7>;
                   temp8  := <temp8>;
                   temp9  := <temp9>;
                   <name> := <formula>;
                 end;
</auxiliaries>
               end;

<auxiliaries vertLoc=SED; calcAfterProcesses=0; calcBeforeZIntegral=1; isZIntegral=0; iterations=0>
                // <description> :
                temp1  := <temp1>;
                temp2  := <temp2>;
                temp3  := <temp3>;
                temp4  := <temp4>;
                temp5  := <temp5>;
                temp6  := <temp6>;
                temp7  := <temp7>;
                temp8  := <temp8>;
                temp9  := <temp9>;
                <name> := <formula> ;
                
</auxiliaries>
             end;
             
             if k = 1 then
             begin
               //initialize auxiliaries for iterative loop
<auxiliaries vertLoc=SUR; calcAfterProcesses=0; calcBeforeZIntegral=1; iterations/=0>
               <name> := <iterInit>;
</auxiliaries>

               //iterative loop follows
               for cgt_iteration:=1 to <maxIterations> do
               begin
<auxiliaries vertLoc=SUR; calcAfterProcesses=0; calcBeforeZIntegral=1; iterations/=0>
                 // <description> :
                 if cgt_iteration <= <iterations> then
                 begin
                   temp1  := <temp1>;
                   temp2  := <temp2>;
                   temp3  := <temp3>;
                   temp4  := <temp4>;
                   temp5  := <temp5>;
                   temp6  := <temp6>;
                   temp7  := <temp7>;
                   temp8  := <temp8>;
                   temp9  := <temp9>;
                   <name> := <formula>;
                 end;
</auxiliaries>
               end;

<auxiliaries vertLoc=SUR; calcAfterProcesses=0; calcBeforeZIntegral=1; iterations=0>
                // <description> :
                temp1  := <temp1>;
                temp2  := <temp2>;
                temp3  := <temp3>;
                temp4  := <temp4>;
                temp5  := <temp5>;
                temp6  := <temp6>;
                temp7  := <temp7>;
                temp8  := <temp8>;
                temp9  := <temp9>;                
                <name> := <formula> ;
                
</auxiliaries>
             end;
             
             //------------------------------------
             // STEP 0.4: add contribution of the current layer k to the zIntegral
             //------------------------------------
<auxiliaries isZIntegral=1; calcAfterProcesses=0>             
             <name> := <name> + (<formula>)*cgt_cellheight*cgt_density;
</auxiliaries>
    end;

    // initialize isZIntegral auxiliary variables with zero
<auxiliaries isZIntegral=1; calcAfterProcesses=1>
    <name> := 0.0; 
</auxiliaries>


    //initialize the variable which cumulates the change of the vertLoc=FIS tracer in each k layer with zero
<tracers vertLoc=FIS>
    cumulated_change_of_<name> := 0.0;
</tracers>


    cgt_bottomdepth := 0.0;
    for k := 1 to kmax do
    begin
       cgt_bottomdepth := cgt_bottomdepth + cellheights[k-1];
          
             //------------------------------------
             // STEP 1: prepare abiotic parameters
             //------------------------------------
             cgt_temp       := forcing_vector_temperature[k-1];         // potential temperature     [Celsius]
             cgt_sali       := forcing_vector_salinity[k-1];            // salinity                  [g/kg]
             cgt_light      := forcing_vector_light[k-1];               // light intensity           [W/m2]
             cgt_cellheight := cellheights[k-1];                        // cell height               [m]
             cgt_density    := density_water;                           // density                   [kg/m3]
             cgt_timestep   := timestep;                                // timestep                  [days]
             cgt_latitude   := location.latitude;                       // geographic latitude       [deg]
             cgt_longitude  := location.longitude;                      // geographic longitude      [deg]
             if k = kmax then
                cgt_current_wave_stress:=forcing_scalar_bottom_stress;  // bottom stress             [N/m2]        
             
             //------------------------------------
             // STEP 2: load tracer values
             //------------------------------------
<tracers vertLoc=WAT>
             <name> := tracer_vector_<name>[k-1]; // <description>
             if k = 1 then
                above_<name> := tracer_vector_<name>[k-1]
             else
                above_<name> := tracer_vector_<name>[k-2];
</tracers>             
<tracers vertLoc=FIS>
             <name> := tracer_scalar_<name>; // <description>
</tracers>             
<auxiliaries vertLoc=WAT; isUsedElsewhere=1>
             <name> := auxiliary_vector_<name>[k-1]; // <description>
</auxiliaries>

<tracers vertLoc=WAT; isPositive=1>
             <name>       := max(<name>,0.0);
             above_<name> := max(above_<name>,0.0);
</tracers>
<tracers vertLoc=FIS; isPositive=1>
             <name>       := max(<name>,0.0);
</tracers>

             if k = kmax then
             begin 
<tracers vertLoc=SED>
                <name> := tracer_scalar_<name>; // <description>
</tracers>

<tracers vertLoc=SED; isPositive=1>
                <name> := max(<name>,0.0);
</tracers>
             end;

             //------------------------------------
             // STEP 4.1: calculate auxiliaries
             //------------------------------------
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=1>
             // <description> :
             <name> := (above_<formula>-<formula>)/cellheights[k-1];

</auxiliaries>

             //initialize auxiliaries for iterative loop
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; iterations/=0>
             <name> := <iterInit>;
</auxiliaries>

             //iterative loop follows
             for cgt_iteration:=1 to <maxIterations> do
             begin
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isGradient=0; iterations/=0>
               // <description> :
               if cgt_iteration <= <iterations> then
               begin
                 temp1  := <temp1>;
                 temp2  := <temp2>;
                 temp3  := <temp3>;
                 temp4  := <temp4>;
                 temp5  := <temp5>;
                 temp6  := <temp6>;
                 temp7  := <temp7>;
                 temp8  := <temp8>;
                 temp9  := <temp9>;
                 <name> := <formula>;
               end;
</auxiliaries>
             end;

<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; iterations=0>
             // <description> :
             temp1  := <temp1>;
             temp2  := <temp2>;
             temp3  := <temp3>;
             temp4  := <temp4>;
             temp5  := <temp5>;
             temp6  := <temp6>;
             temp7  := <temp7>;
             temp8  := <temp8>;
             temp9  := <temp9>;
             <name> := <formula>;
             
</auxiliaries>

             if k = kmax then
             begin
               //initialize auxiliaries for iterative loop
<auxiliaries vertLoc=SED; calcAfterProcesses=0; isZIntegral=0; iterations/=0>
               <name> := <iterInit>;
</auxiliaries>

               //iterative loop follows
               for cgt_iteration:=1 to <maxIterations> do
               begin
<auxiliaries vertLoc=SED; calcAfterProcesses=0; isZIntegral=0; iterations/=0>
                 // <description> :
                 if cgt_iteration <= <iterations> then
                 begin
                   temp1  := <temp1>;
                   temp2  := <temp2>;
                   temp3  := <temp3>;
                   temp4  := <temp4>;
                   temp5  := <temp5>;
                   temp6  := <temp6>;
                   temp7  := <temp7>;
                   temp8  := <temp8>;
                   temp9  := <temp9>;
                   <name> := <formula>;
                 end;
</auxiliaries>
               end;

<auxiliaries vertLoc=SED; calcAfterProcesses=0; isZIntegral=0; iterations=0>
                // <description> :
                temp1  := <temp1>;
                temp2  := <temp2>;
                temp3  := <temp3>;
                temp4  := <temp4>;
                temp5  := <temp5>;
                temp6  := <temp6>;
                temp7  := <temp7>;
                temp8  := <temp8>;
                temp9  := <temp9>;
                <name> := <formula> ;
                
</auxiliaries>
             end;
             
             if k = 1 then
             begin
               //initialize auxiliaries for iterative loop
<auxiliaries vertLoc=SUR; calcAfterProcesses=0; iterations/=0>
               <name> := <iterInit>;
</auxiliaries>

               //iterative loop follows
               for cgt_iteration:=1 to <maxIterations> do
               begin
<auxiliaries vertLoc=SUR; calcAfterProcesses=0; iterations/=0>
                 // <description> :
                 if cgt_iteration <= <iterations> then
                 begin
                   temp1  := <temp1>;
                   temp2  := <temp2>;
                   temp3  := <temp3>;
                   temp4  := <temp4>;
                   temp5  := <temp5>;
                   temp6  := <temp6>;
                   temp7  := <temp7>;
                   temp8  := <temp8>;
                   temp9  := <temp9>;
                   <name> := <formula>;
                 end;
</auxiliaries>
               end;

<auxiliaries vertLoc=SUR; calcAfterProcesses=0; iterations=0>
                // <description> :
                temp1  := <temp1>;
                temp2  := <temp2>;
                temp3  := <temp3>;
                temp4  := <temp4>;
                temp5  := <temp5>;
                temp6  := <temp6>;
                temp7  := <temp7>;
                temp8  := <temp8>;
                temp9  := <temp9>;
                <name> := <formula> ;
                
</auxiliaries>
             end;
             
             //------------------------------------
             // STEP 4.2: output of auxiliaries
             //------------------------------------
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isOutput=1>             
             output_vector_<name>[k-1] := output_vector_<name>[k-1] + <name>;
</auxiliaries>

             if k = kmax then
             begin
<auxiliaries vertLoc=SED; calcAfterProcesses=0; isOutput=1>
                   output_scalar_<name> := output_scalar_<name> + <name>;
</auxiliaries>
             end;
             if k = 1 then
             begin
<auxiliaries vertLoc=SUR; calcAfterProcesses=0; isOutput=1>
                   output_scalar_<name> := output_scalar_<name> + <name>;
</auxiliaries>
             end;

             //------------------------------------
             // STEP 5: calculate process limitations
             //------------------------------------

<tracers vertLoc=WAT>
  <limitations>
             <name> := <formula>;
  </limitations>
</tracers>

             if k = kmax then
             begin
<tracers vertLoc=SED>
  <limitations>
                <name> := <formula>;
  </limitations>
</tracers>
             end;

             if k = 1 then
             begin
<tracers vertLoc=SUR>
  <limitations>
                <name> := <formula>;
  </limitations>
</tracers>
             end;

<processes>
             total_rate_<name>          := 0.0;
</processes>
             number_of_loop := 1;

//             PositiveEulerTimestep(
             if useSavedRates then
               PatankarTimestep(
             // input parameters
             <tracers vertLoc=WAT>
                                        <name>,
                                        patankar_modification_<name>[k-1],
             </tracers>
             <tracers vertLoc/=WAT>
                                        <name>,
                                        patankar_modification_<name>,
             </tracers>
             <auxiliaries>
                                        <name>,
             </auxiliaries>
             <processes vertLoc=WAT>
                                        saved_rate_<name>[k-1],
             </processes>
             <processes vertLoc/=WAT>
                                        saved_rate_<name>,
             </processes>
                                        cgt_temp                ,
                                        cgt_sali                ,
                                        cgt_light               ,
                                        cgt_cellheight          ,
                                        cgt_density             ,
                                        cgt_longitude           ,
                                        cgt_latitude            ,
                                        cgt_current_wave_stress ,
                                        cgt_bottomdepth         ,
                                        cgt_year                ,
                                        cgt_dayofyear           ,
                                        cgt_hour                ,
                                        cgt_iteration           ,
                                        k, kmax,
             //input/output parameters
                                        cgt_timestep,
             <tracers>
               <limitations>
                                        <name>,
               </limitations>
             </tracers>
             //output parameters
             <processes>
                                        total_rate_<name>,
             </processes>
             <tracers vertLoc=WAT>
                                        tracer_vector_<name>[k-1],
                                        
             </tracers>
             <tracers vertLoc/=WAT>
                                        tracer_scalar_<name>,
             </tracers>
             <processes vertLoc=WAT; isOutput=1>
                                        output_vector_<name>[k-1],
             </processes>
             <processes vertLoc/=WAT; isOutput=1>
                                        output_scalar_<name>,
             </processes>
                                        number_of_loop
                                        )
             else
               PatankarTimestep(
             // input parameters
             <tracers vertLoc=WAT>
                                        <name>,
                                        patankar_modification_<name>[k-1],
             </tracers>
             <tracers vertLoc/=WAT>
                                        <name>,
                                        patankar_modification_<name>,
             </tracers>
             <auxiliaries>
                                        <name>,
             </auxiliaries>
             <processes vertLoc=WAT>
                                        -1.0,
             </processes>
             <processes vertLoc/=WAT>
                                        -1.0,
             </processes>
                                        cgt_temp                ,
                                        cgt_sali                ,
                                        cgt_light               ,
                                        cgt_cellheight          ,
                                        cgt_density             ,
                                        cgt_longitude           ,
                                        cgt_latitude            ,
                                        cgt_current_wave_stress ,
                                        cgt_bottomdepth         ,
                                        cgt_year                ,
                                        cgt_dayofyear           ,
                                        cgt_hour                ,
                                        cgt_iteration           ,
                                        k, kmax,
             //input/output parameters
                                        cgt_timestep,
             <tracers>
               <limitations>
                                        <name>,
               </limitations>
             </tracers>
             //output parameters
             <processes>
                                        total_rate_<name>,
             </processes>
             <tracers vertLoc=WAT>
                                        tracer_vector_<name>[k-1],
                                        
             </tracers>
             <tracers vertLoc/=WAT>
                                        tracer_scalar_<name>,
             </tracers>
             <processes vertLoc=WAT; isOutput=1>
                                        output_vector_<name>[k-1],
             </processes>
             <processes vertLoc/=WAT; isOutput=1>
                                        output_scalar_<name>,
             </processes>
                                        number_of_loop
                                        );

                                       
             //------------------------------------
             // STEP 7.0: add cumulated change to vertLoc=FIS tracers
             //------------------------------------
             
             if k = kmax then
             begin
<tracers vertLoc=FIS>
               // apply change of <description>:
               tracer_scalar_<name> := tracer_scalar_<name> + cumulated_change_of_<name>;
               <name> := tracer_scalar_<name>;
</tracers>
             end;

             //------------------------------------
             // STEP 7.1: output of new tracer concentrations
             //------------------------------------
<tracers vertLoc=WAT; isOutput=1>
             output_vector_<name>[k-1] := output_vector_<name>[k-1] + <name>;
</tracers>
             if k = kmax then
             begin
<tracers vertLoc=SED; isOutput=1>
               output_scalar_<name> := output_scalar_<name> + <name>;
</tracers>
<tracers vertLoc=FIS; isOutput=1>
               output_scalar_<name> := output_scalar_<name> + <name>;
</tracers>
             end;
             if k=1 then
             begin
<tracers vertLoc=SUR; isOutput=1>
               output_scalar_<name> := output_scalar_<name> + <name>;
</tracers>
             end;
             
             //------------------------------------
             // STEP 7.2: calculate "late" auxiliaries
             //------------------------------------
<auxiliaries vertLoc=WAT; calcAfterProcesses=1; isZGradient=0>
             // <description> :
             temp1  := <temp1>;
             temp2  := <temp2>;
             temp3  := <temp3>;
             temp4  := <temp4>;
             temp5  := <temp5>;
             temp6  := <temp6>;
             temp7  := <temp7>;
             temp8  := <temp8>;
             temp9  := <temp9>;
             <name> := <formula>;
             
</auxiliaries>

             if k = kmax then
             begin
<auxiliaries vertLoc=SED; calcAfterProcesses=1; isZIntegral=0>
                // <description> :
                temp1  := <temp1>;
                temp2  := <temp2>;
                temp3  := <temp3>;
                temp4  := <temp4>;
                temp5  := <temp5>;
                temp6  := <temp6>;
                temp7  := <temp7>;
                temp8  := <temp8>;
                temp9  := <temp9>;
                <name> := <formula>;
                
</auxiliaries>
             end;
             
             if k = 1 then
             begin
<auxiliaries vertLoc=SUR; calcAfterProcesses=1>
                // <description> :
                temp1  := <temp1>;
                temp2  := <temp2>;
                temp3  := <temp3>;
                temp4  := <temp4>;
                temp5  := <temp5>;
                temp6  := <temp6>;
                temp7  := <temp7>;
                temp8  := <temp8>;
                temp9  := <temp9>;
                <name> := <formula>;
                
</auxiliaries>
             end;

             //------------------------------------
             // STEP 7.3: add values from this k level to auxiliary variables with isZIntegral=1
             //------------------------------------
<auxiliaries vertLoc=SED; calcAfterProcesses=1; isZIntegral=1>
             <name> := <name> + (<formula>)*cgt_cellheight*cgt_density;
</auxiliaries>

             //------------------------------------
             // STEP 7.4: output of "late" auxiliaries
             //------------------------------------
<auxiliaries vertLoc=WAT; calcAfterProcesses=1; isOutput=1>             
             output_vector_<name>[k-1] := output_vector_<name>[k-1] + <name>;
</auxiliaries>
<auxiliaries vertLoc=WAT; calcAfterProcesses=1; isUsedElsewhere=1>             
             auxiliary_vector_<name>[k-1] := <name>;
</auxiliaries>
             if k = kmax then
             begin
<auxiliaries vertLoc=SED; calcAfterProcesses=1; isOutput=1>
               output_scalar_<name> := output_scalar_<name> + <name>;
</auxiliaries>
<auxiliaries vertLoc=SED; calcAfterProcesses=1; isUsedElsewhere=1>             
               auxiliary_scalar_<name> := <name>;
</auxiliaries>
             end;
             if k = 1 then
             begin
<auxiliaries vertLoc=SUR; calcAfterProcesses=1; isOutput=1>
               output_scalar_<name> := output_scalar_<name> + <name>;
</auxiliaries>
<auxiliaries vertLoc=SUR; calcAfterProcesses=1; isUsedElsewhere=1>             
               auxiliary_scalar_<name> := <name>;
</auxiliaries>
             end;

             //---------------------------------------
             // STEP 7.5: passing vertical velocity and diffusivity to the coupler
             //---------------------------------------

             // EXPLICIT MOVEMENT
             <tracers vertLoc=WAT; vertSpeed/=0>
                vertical_speed_of_<name>[k-1]:=(<vertSpeed>)/(24*3600.0); // convert to m/s
                vertical_diffusivity_of_<name>[k-1]:=(<vertDiff>);        // leave as m2/s
             </tracers> 
    end;
    
    //---------------------------------------
    // biological timestep has ended
    //---------------------------------------
    
    //---------------------------------------
    // vertical movement follows
    //---------------------------------------
    
    // calculate new total marked element concentrations
    for k := 1 to kmax do
    begin
          <celements>
             tracer_vector_<total>[k-1] := 
             <containingTracers vertLoc=WAT>
                max(0.0,tracer_vector_<ct>[k-1])*<ctAmount> + 
             </containingTracers>
                0.0;
          </celements>
    end;
    
    // vertical movement of tracers
    for m := 1 to num_vmove_steps do
    begin
       // store the old age concentrations
          <celements isAging/=0>
          for k:=1 to kmax do
             old_vector_<aged>[k-1] := tracer_vector_<aged>[k-1];
          </celements>
       // first, move the age concentration of marked elements
       <tracers childOf/=none; vertLoc=WAT; vertSpeed/=0; hasCeAged>
         vmove_explicit(vertical_speed_of_<name>, 
                         tracer_vector_<ceAgedName>, old_vector_<ceAgedName>, 
                         tracer_vector_<name>,<ceAmount>,
                         tracer_vector_<ceTotalName>, 
                         cellheights, timestep/num_vmove_steps*(24*3600));

       </tracers>
       // second, move the tracers (including marked tracers) themselves
       <tracers vertLoc=WAT; vertSpeed/=0>
         vmove_explicit(vertical_speed_of_<name>, 
                                   tracer_vector_<name>, tracer_vector_<name>, 
                                   tracer_vector_<name>, 1.0, 
                                   tracer_vector_<name>, 
                                   cellheights, timestep/num_vmove_steps*(24*3600));
       </tracers>
       // third, calculate new total marked element concentrations
       for k := 1 to kmax do
       begin
             <celements>
                tracer_vector_<total>[k-1] := 
                <containingTracers vertLoc=WAT>
                   max(0.0,tracer_vector_<ct>[k-1])*<ctAmount> + 
                </containingTracers>
                   0.0;
             </celements>       
       end;
    end;
    // vertical diffusion of tracers
    for m := 1 to num_vmove_steps do
    begin
       // store the old age concentrations
          <celements isAging/=0>
          for k:=1 to kmax do
             old_vector_<aged>[k-1] := tracer_vector_<aged>[k-1];
          </celements>
       // first, diffuse the age concentration of marked elements
       <tracers childOf/=none; vertLoc=WAT; vertSpeed/=0; hasCeAged>
          vdiff_explicit(vertical_diffusivity_of_<name>, 
                         tracer_vector_<ceAgedName>, old_vector_<ceAgedName>, 
                         tracer_vector_<name>,<ceAmount>,
                         tracer_vector_<ceTotalName>, 
                         cellheights, timestep/num_vmove_steps*(24*3600));

       </tracers>
       // second, diffuse the tracers (including marked tracers) themselves
       <tracers vertLoc=WAT; vertSpeed/=0>
          vdiff_explicit(vertical_diffusivity_of_<name>, 
                                   tracer_vector_<name>, 
                                   cellheights, timestep/num_vmove_steps*(24*3600));
       </tracers>
       // third, calculate new total marked element concentrations
       for k := 1 to kmax do
       begin
             <celements>
                tracer_vector_<total>[k-1] := 
                <containingTracers vertLoc=WAT>
                   max(0.0,tracer_vector_<ct>[k-1])*<ctAmount> + 
                </containingTracers>
                   0.0;
             </celements>       
       end;
    end;

    // calculate total colored element concentrations at bottom
    <celements>
       tracer_scalar_<totalBottom> := 
       <containingTracers vertLoc=SED>
          max(0.0,tracer_scalar_<name>)*<ctAmount> + 
       </containingTracers>
          0.0;
    </celements>
end;

procedure cgt_bio_timestep_intermediate;
var
  k, m: Integer;

  cgt_temp                : Double;           // potential temperature     [Celsius]
  cgt_sali                : Double;           // salinity                  [g/kg]
  cgt_light               : Double;           // light intensity           [W/m2]
  cgt_cellheight          : Double;           // cell height               [m]
  cgt_density             : Double;           // density                   [kg/m3]
  cgt_timestep            : Double;           // timestep                  [days]
  cgt_longitude           : Double;           // geographic longitude      [deg]
  cgt_latitude            : Double;           // geographic latitude       [deg]
  cgt_current_wave_stress : Double;           // bottom stress             [N/m2]
  cgt_bottomdepth         : Double;           // bottom depth              [m]
  cgt_year                : Double;           // year (integer value)      [years]
  cgt_dayofyear           : Double;           // julian day of the year (integer value) [days]
  cgt_hour                : Double;           // hour plus fraction (0..23.99) [hours]
  cgt_iteration           : Integer;          // number of iteration in iterative loop [1]

  number_of_loop            : Integer;

  temp1                     : Double;
  temp2                     : Double;
  temp3                     : Double;
  temp4                     : Double;
  temp5                     : Double;
  temp6                     : Double;
  temp7                     : Double;
  temp8                     : Double;
  temp9                     : Double;

  <tracers>
    <name>       : Double; // <description>
    <limitations>
      <name> : Double;
    </limitations>
  </tracers>
  <auxiliaries>
    <name>       : Double; // <description>
  </auxiliaries>
  <processes>
    total_rate_<name> : Double;
  </processes>
  <tracers vertLoc=WAT>
    above_<name> : Double; 
  </tracers>
  <tracers vertLoc=FIS>
    cumulated_change_of_<name> : Double;
  </tracers>
begin
    // calculate total element concentrations in the water column
    for k := 1 to kmax do
    begin
          <celements>
             tracer_vector_<total>[k-1] := 
             <containingTracers vertLoc=WAT>
                max(0.0,tracer_vector_<ct>[k-1])*<ctAmount> + 
             </containingTracers>
                0.0;
          </celements>   
    end;

    // calculate total colored element concentrations at bottom
       <celements>
          tracer_scalar_<totalBottom> := 
          <containingTracers vertLoc=SED>
             max(0.0,tracer_scalar_<ct>)*<ctAmount> + 
          </containingTracers>
             0.0;
       </celements>    

        // First, do the Pre-Loop to calculate isZIntegral=1 auxiliaries

    // initialize isZIntegral auxiliary variables with zero
    <auxiliaries isZIntegral=1; calcAfterProcesses=0>
       <name> := 0.0; 
    </auxiliaries>

    cgt_year     :=yearOf(current_date);
    cgt_dayofyear:=dayOfTheYear(current_date);
    cgt_hour     :=hourOf(current_date)+minuteOf(current_date)/60+secondOf(current_date)/3600;

    cgt_bottomdepth := 0.0;
    for k := 1 to kmax do
    begin
       cgt_bottomdepth := cgt_bottomdepth + cellheights[k-1];
          
             //------------------------------------
             // STEP 0.1: prepare abiotic parameters
             //------------------------------------
             cgt_temp       := forcing_vector_temperature[k-1];         // potential temperature     [Celsius]
             cgt_sali       := forcing_vector_salinity[k-1];            // salinity                  [g/kg]
             cgt_light      := forcing_vector_light[k-1];               // light intensity           [W/m2]
             cgt_cellheight := cellheights[k-1];                        // cell height               [m]
             cgt_density    := density_water;                           // density                   [kg/m3]
             cgt_timestep   := timestep;                                // timestep                  [days]
             cgt_longitude  := location.longitude;                      // geographic longitude      [deg]
             cgt_latitude   := location.latitude;                       // geographic latitude       [deg]
             if k = kmax then
                cgt_current_wave_stress:=forcing_scalar_bottom_stress;  // bottom stress             [N/m2]
             
             //------------------------------------
             // STEP 0.2: load tracer values
             //------------------------------------
<tracers vertLoc=WAT; calcBeforeZIntegral=1>
             <name> := tracer_vector_<name>[k-1]; // <description>
             if k = 1 then
                above_<name> := tracer_vector_<name>[k-1]
             else
                above_<name> := tracer_vector_<name>[k-2];
</tracers>
<tracers vertLoc=FIS; calcBeforeZIntegral=1>
             <name> := tracer_scalar_<name>; // <description>
</tracers>             
<auxiliaries vertLoc=WAT; isUsedElsewhere=1; calcBeforeZIntegral=1>
             <name> := auxiliary_vector_<name>[k-1]; // <description>
</auxiliaries>

<tracers vertLoc=WAT; isPositive=1; calcBeforeZIntegral=1>
             <name>       := max(<name>,0.0);
             above_<name> := max(above_<name>,0.0);
</tracers>
<tracers vertLoc=FIS; isPositive=1; calcBeforeZIntegral=1>
             <name>       := max(<name>,0.0);
</tracers>

             if k = kmax then
             begin
<tracers vertLoc=SED; calcBeforeZIntegral=1>
                <name> := tracer_scalar_<name>; // <description>
</tracers>

<tracers vertLoc=SED; isPositive=1; calcBeforeZIntegral=1>
                <name> := max(<name>,0.0);
</tracers>
             end;

             //------------------------------------
             // STEP 0.3: calculate auxiliaries
             //------------------------------------

<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=1; calcBeforeZIntegral=1>
             // <description> :
             <name> := (above_<formula>-<formula>)/cgt_cellheight;

</auxiliaries>

             //initialize auxiliaries for iterative loop
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; calcBeforeZIntegral=1; iterations/=0>
             <name> := <iterInit>;
</auxiliaries>

             //iterative loop follows
             for cgt_iteration:=1 to <maxIterations> do
             begin
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; calcBeforeZIntegral=1; iterations/=0>
               // <description> :
               if cgt_iteration <= <iterations> then
               begin
                 temp1  := <temp1>;
                 temp2  := <temp2>;
                 temp3  := <temp3>;
                 temp4  := <temp4>;
                 temp5  := <temp5>;
                 temp6  := <temp6>;
                 temp7  := <temp7>;
                 temp8  := <temp8>;
                 temp9  := <temp9>;
                 <name> := <formula>;
               end;
</auxiliaries>
             end;

<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; calcBeforeZIntegral=1; iterations=0>
             // <description> :
             temp1  := <temp1>;
             temp2  := <temp2>;
             temp3  := <temp3>;
             temp4  := <temp4>;
             temp5  := <temp5>;
             temp6  := <temp6>;
             temp7  := <temp7>;
             temp8  := <temp8>;
             temp9  := <temp9>;
             <name> := <formula>;
             
</auxiliaries>

             if k = kmax then
             begin
               //initialize auxiliaries for iterative loop
<auxiliaries vertLoc=SED; calcAfterProcesses=0; isZIntegral=0; calcBeforeZIntegral=1; iterations/=0>
               <name> := <iterInit>;
</auxiliaries>

               //iterative loop follows
               for cgt_iteration:=1 to <maxIterations> do
               begin
<auxiliaries vertLoc=SED; calcAfterProcesses=0; isZIntegral=0; calcBeforeZIntegral=1; iterations/=0>
                 // <description> :
                 if cgt_iteration <= <iterations> then
                 begin
                   temp1  := <temp1>;
                   temp2  := <temp2>;
                   temp3  := <temp3>;
                   temp4  := <temp4>;
                   temp5  := <temp5>;
                   temp6  := <temp6>;
                   temp7  := <temp7>;
                   temp8  := <temp8>;
                   temp9  := <temp9>;
                   <name> := <formula>;
                 end;
</auxiliaries>
               end;

<auxiliaries vertLoc=SED; calcAfterProcesses=0; calcBeforeZIntegral=1; isZIntegral=0; iterations=0>
                // <description> :
                temp1  := <temp1>;
                temp2  := <temp2>;
                temp3  := <temp3>;
                temp4  := <temp4>;
                temp5  := <temp5>;
                temp6  := <temp6>;
                temp7  := <temp7>;
                temp8  := <temp8>;
                temp9  := <temp9>;
                <name> := <formula> ;
                
</auxiliaries>
             end;
             
             if k = 1 then
             begin
               //initialize auxiliaries for iterative loop
<auxiliaries vertLoc=SUR; calcAfterProcesses=0; calcBeforeZIntegral=1; iterations/=0>
               <name> := <iterInit>;
</auxiliaries>

               //iterative loop follows
               for cgt_iteration:=1 to <maxIterations> do
               begin
<auxiliaries vertLoc=SUR; calcAfterProcesses=0; calcBeforeZIntegral=1; iterations/=0>
                 // <description> :
                 if cgt_iteration <= <iterations> then
                 begin
                   temp1  := <temp1>;
                   temp2  := <temp2>;
                   temp3  := <temp3>;
                   temp4  := <temp4>;
                   temp5  := <temp5>;
                   temp6  := <temp6>;
                   temp7  := <temp7>;
                   temp8  := <temp8>;
                   temp9  := <temp9>;
                   <name> := <formula>;
                 end;
</auxiliaries>
               end;

<auxiliaries vertLoc=SUR; calcAfterProcesses=0; calcBeforeZIntegral=1; iterations=0>
                // <description> :
                temp1  := <temp1>;
                temp2  := <temp2>;
                temp3  := <temp3>;
                temp4  := <temp4>;
                temp5  := <temp5>;
                temp6  := <temp6>;
                temp7  := <temp7>;
                temp8  := <temp8>;
                temp9  := <temp9>;                
                <name> := <formula> ;
                
</auxiliaries>
             end;
             
             //------------------------------------
             // STEP 0.4: add contribution of the current layer k to the zIntegral
             //------------------------------------
<auxiliaries isZIntegral=1; calcAfterProcesses=0>             
             <name> := <name> + (<formula>)*cgt_cellheight*cgt_density;
</auxiliaries>
    end;

    // initialize isZIntegral auxiliary variables with zero
<auxiliaries isZIntegral=1; calcAfterProcesses=1>
    <name> := 0.0; 
</auxiliaries>


    //initialize the variable which cumulates the change of the vertLoc=FIS tracer in each k layer with zero
<tracers vertLoc=FIS>
    cumulated_change_of_<name> := 0.0;
</tracers>


    cgt_bottomdepth := 0.0;
    for k := 1 to kmax do
    begin
       cgt_bottomdepth := cgt_bottomdepth + cellheights[k-1];
          
             //------------------------------------
             // STEP 1: prepare abiotic parameters
             //------------------------------------
             cgt_temp       := forcing_vector_temperature[k-1];         // potential temperature     [Celsius]
             cgt_sali       := forcing_vector_salinity[k-1];            // salinity                  [g/kg]
             cgt_light      := forcing_vector_light[k-1];               // light intensity           [W/m2]
             cgt_cellheight := cellheights[k-1];                        // cell height               [m]
             cgt_density    := density_water;                           // density                   [kg/m3]
             cgt_timestep   := timestep;                                // timestep                  [days]
             cgt_latitude   := location.latitude;                       // geographic latitude       [deg]
             cgt_longitude  := location.longitude;                      // geographic longitude      [deg]
             if k = kmax then
                cgt_current_wave_stress:=forcing_scalar_bottom_stress;  // bottom stress             [N/m2]        
             
             //------------------------------------
             // STEP 2: load tracer values
             //------------------------------------
<tracers vertLoc=WAT>
             <name> := tracer_vector_<name>[k-1]; // <description>
             if k = 1 then
                above_<name> := tracer_vector_<name>[k-1]
             else
                above_<name> := tracer_vector_<name>[k-2];
</tracers>             
<tracers vertLoc=FIS>
             <name> := tracer_scalar_<name>; // <description>
</tracers>             
<auxiliaries vertLoc=WAT; isUsedElsewhere=1>
             <name> := auxiliary_vector_<name>[k-1]; // <description>
</auxiliaries>

<tracers vertLoc=WAT; isPositive=1>
             <name>       := max(<name>,0.0);
             above_<name> := max(above_<name>,0.0);
</tracers>
<tracers vertLoc=FIS; isPositive=1>
             <name>       := max(<name>,0.0);
</tracers>

             if k = kmax then
             begin 
<tracers vertLoc=SED>
                <name> := tracer_scalar_<name>; // <description>
</tracers>

<tracers vertLoc=SED; isPositive=1>
                <name> := max(<name>,0.0);
</tracers>
             end;

             //------------------------------------
             // STEP 4.1: calculate auxiliaries
             //------------------------------------
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=1>
             // <description> :
             <name> := (above_<formula>-<formula>)/cellheights[k-1];

</auxiliaries>

             //initialize auxiliaries for iterative loop
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; iterations/=0>
             <name> := <iterInit>;
</auxiliaries>

             //iterative loop follows
             for cgt_iteration:=1 to <maxIterations> do
             begin
<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isGradient=0; iterations/=0>
               // <description> :
               if cgt_iteration <= <iterations> then
               begin
                 temp1  := <temp1>;
                 temp2  := <temp2>;
                 temp3  := <temp3>;
                 temp4  := <temp4>;
                 temp5  := <temp5>;
                 temp6  := <temp6>;
                 temp7  := <temp7>;
                 temp8  := <temp8>;
                 temp9  := <temp9>;
                 <name> := <formula>;
               end;
</auxiliaries>
             end;

<auxiliaries vertLoc=WAT; calcAfterProcesses=0; isZGradient=0; iterations=0>
             // <description> :
             temp1  := <temp1>;
             temp2  := <temp2>;
             temp3  := <temp3>;
             temp4  := <temp4>;
             temp5  := <temp5>;
             temp6  := <temp6>;
             temp7  := <temp7>;
             temp8  := <temp8>;
             temp9  := <temp9>;
             <name> := <formula>;
             
</auxiliaries>

             if k = kmax then
             begin
               //initialize auxiliaries for iterative loop
<auxiliaries vertLoc=SED; calcAfterProcesses=0; isZIntegral=0; iterations/=0>
               <name> := <iterInit>;
</auxiliaries>

               //iterative loop follows
               for cgt_iteration:=1 to <maxIterations> do
               begin
<auxiliaries vertLoc=SED; calcAfterProcesses=0; isZIntegral=0; iterations/=0>
                 // <description> :
                 if cgt_iteration <= <iterations> then
                 begin
                   temp1  := <temp1>;
                   temp2  := <temp2>;
                   temp3  := <temp3>;
                   temp4  := <temp4>;
                   temp5  := <temp5>;
                   temp6  := <temp6>;
                   temp7  := <temp7>;
                   temp8  := <temp8>;
                   temp9  := <temp9>;
                   <name> := <formula>;
                 end;
</auxiliaries>
               end;

<auxiliaries vertLoc=SED; calcAfterProcesses=0; isZIntegral=0; iterations=0>
                // <description> :
                temp1  := <temp1>;
                temp2  := <temp2>;
                temp3  := <temp3>;
                temp4  := <temp4>;
                temp5  := <temp5>;
                temp6  := <temp6>;
                temp7  := <temp7>;
                temp8  := <temp8>;
                temp9  := <temp9>;
                <name> := <formula> ;
                
</auxiliaries>
             end;
             
             if k = 1 then
             begin
               //initialize auxiliaries for iterative loop
<auxiliaries vertLoc=SUR; calcAfterProcesses=0; iterations/=0>
               <name> := <iterInit>;
</auxiliaries>

               //iterative loop follows
               for cgt_iteration:=1 to <maxIterations> do
               begin
<auxiliaries vertLoc=SUR; calcAfterProcesses=0; iterations/=0>
                 // <description> :
                 if cgt_iteration <= <iterations> then
                 begin
                   temp1  := <temp1>;
                   temp2  := <temp2>;
                   temp3  := <temp3>;
                   temp4  := <temp4>;
                   temp5  := <temp5>;
                   temp6  := <temp6>;
                   temp7  := <temp7>;
                   temp8  := <temp8>;
                   temp9  := <temp9>;
                   <name> := <formula>;
                 end;
</auxiliaries>
               end;

<auxiliaries vertLoc=SUR; calcAfterProcesses=0; iterations=0>
                // <description> :
                temp1  := <temp1>;
                temp2  := <temp2>;
                temp3  := <temp3>;
                temp4  := <temp4>;
                temp5  := <temp5>;
                temp6  := <temp6>;
                temp7  := <temp7>;
                temp8  := <temp8>;
                temp9  := <temp9>;
                <name> := <formula> ;
                
</auxiliaries>
             end;
             
             //------------------------------------
             // STEP 4.2: output of auxiliaries
             //------------------------------------

             //------------------------------------
             // STEP 5: calculate process limitations
             //------------------------------------

<tracers vertLoc=WAT>
  <limitations>
             <name> := <formula>;
  </limitations>
</tracers>

             if k = kmax then
             begin
<tracers vertLoc=SED>
  <limitations>
                <name> := <formula>;
  </limitations>
</tracers>
             end;

             if k = 1 then
             begin
<tracers vertLoc=SUR>
  <limitations>
                <name> := <formula>;
  </limitations>
</tracers>
             end;

<processes>
             total_rate_<name>          := 0.0;
</processes>
             number_of_loop := 1;

//             PositiveEulerTimestep(
             PatankarTimestepIntermediate(
             // input parameters
             <tracers>
                                        <name>,
             </tracers>
             <auxiliaries>
                                        <name>,
             </auxiliaries>
                                        cgt_temp                ,
                                        cgt_sali                ,
                                        cgt_light               ,
                                        cgt_cellheight          ,
                                        cgt_density             ,
                                        cgt_longitude           ,
                                        cgt_latitude            ,
                                        cgt_current_wave_stress ,
                                        cgt_bottomdepth         ,
                                        cgt_year                ,
                                        cgt_dayofyear           ,
                                        cgt_hour                ,
                                        cgt_iteration           ,
                                        k, kmax,
             //input/output parameters
                                        cgt_timestep,
             <tracers>
               <limitations>
                                        <name>,
               </limitations>
             </tracers>
             //output parameters
             <processes>
                                        total_rate_<name>,
             </processes>
             <tracers vertLoc=WAT>
                                        tracer_vector_<name>[k-1],
                                        
             </tracers>
             <tracers vertLoc/=WAT>
                                        tracer_scalar_<name>,
             </tracers>
                                        number_of_loop
                                        );
                                       
             //------------------------------------
             // STEP 7.0: add cumulated change to vertLoc=FIS tracers
             //------------------------------------
             
             if k = kmax then
             begin
<tracers vertLoc=FIS>
               // apply change of <description>:
               tracer_scalar_<name> := tracer_scalar_<name> + cumulated_change_of_<name>;
               <name> := tracer_scalar_<name>;
</tracers>
             end;

             //------------------------------------
             // STEP 7.1: output of new tracer concentrations
             //------------------------------------
             
             //------------------------------------
             // STEP 7.2: calculate "late" auxiliaries
             //------------------------------------
             //------------------------------------
             // STEP 7.3: add values from this k level to auxiliary variables with isZIntegral=1
             //------------------------------------

             //------------------------------------
             // STEP 7.4: output of "late" auxiliaries
             //------------------------------------

             //---------------------------------------
             // STEP 7.5: passing vertical velocity and diffusivity to the coupler
             //---------------------------------------
    end;
    
    //---------------------------------------
    // biological timestep has ended
    //---------------------------------------
    
    //---------------------------------------
    // vertical movement follows
    //---------------------------------------
    
end;

procedure cgt_mixing_timestep;
var
  i, m: Integer;
begin
  // vertical diffusion of all tracers
  for i:=0 to kmax-1 do
  begin
    forcing_vector_diffusivity[i] := max(forcing_vector_diffusivity[i],min_diffusivity);
    forcing_vector_diffusivity[i] := min(forcing_vector_diffusivity[i],max_diffusivity);
  end;
  for m := 1 to num_vdiff_steps do
  begin
       // diffuse the tracers (including marked tracers) themselves
       <tracers vertLoc=WAT>
          vdiff_explicit(forcing_vector_diffusivity, 
                                   tracer_vector_<name>, 
                                   cellheights, timestep/num_vdiff_steps*(24*3600));
       </tracers>
  end;
end;

procedure cgt_output_final(current_output_index, output_count: Integer);
var
  i: Integer;
begin
  // in the water column
 for i:=0 to kmax-1 do
 begin
  <tracers vertLoc=WAT; isOutput=1>
    output_<name>[i,current_output_index-1]:=output_vector_<name>[i]/output_count;
  </tracers>
  <auxiliaries vertLoc=WAT; isOutput=1>
    output_<name>[i,current_output_index-1]:=output_vector_<name>[i]/output_count;
  </auxiliaries>
  <processes vertLoc=WAT; isOutput=1>
    output_<name>[i,current_output_index-1]:=output_vector_<name>[i]/output_count;
  </processes>
 end;
// in the sediment
<tracers vertLoc=SED; isOutput=1>
  output_<name>[current_output_index-1]:=output_scalar_<name>/output_count;
</tracers>
<auxiliaries vertLoc=SED; isOutput=1>
  output_<name>[current_output_index-1]:=output_scalar_<name>/output_count;
</auxiliaries>
<processes vertLoc=SED; isOutput=1>
  output_<name>[current_output_index-1]:=output_scalar_<name>/output_count;
</processes>
// at the sea surface
<tracers vertLoc=SUR; isOutput=1>
  output_<name>[current_output_index-1]:=output_scalar_<name>/output_count;
</tracers>
<auxiliaries vertLoc=SUR; isOutput=1>
  output_<name>[current_output_index-1]:=output_scalar_<name>/output_count;
</auxiliaries>
<processes vertLoc=SUR; isOutput=1>
  output_<name>[current_output_index-1]:=output_scalar_<name>/output_count;
</processes>
//pseudo-3d tracers
<tracers vertLoc=FIS; isOutput=1>
  output_<name>[current_output_index-1]:=output_scalar_<name>/output_count;
</tracers>

// now, set the temporary output vector to zero
 for i:=0 to kmax-1 do
 begin
<tracers vertLoc=WAT; isOutput=1>
  output_vector_<name>[i] := 0.0;
</tracers>
<auxiliaries vertLoc=WAT; isOutput=1>
  output_vector_<name>[i] := 0.0;
</auxiliaries>
<processes vertLoc=WAT; isOutput=1>
  output_vector_<name>[i] := 0.0;
</processes>
 end;
<tracers vertLoc=SED; isOutput=1>
  output_scalar_<name> := 0.0;        
</tracers>
<auxiliaries vertLoc=SED; isOutput=1>
  output_scalar_<name> := 0.0;        
</auxiliaries>
<processes vertLoc=SED; isOutput=1>
  output_scalar_<name> := 0.0;        
</processes>
<tracers vertLoc=SUR; isOutput=1>
  output_scalar_<name> := 0.0;        
</tracers>
<auxiliaries vertLoc=SUR; isOutput=1>
  output_scalar_<name> := 0.0;        
</auxiliaries>
<processes vertLoc=SUR; isOutput=1>
  output_scalar_<name> := 0.0;        
</processes>
<tracers vertLoc=FIS; isOutput=1>
  output_scalar_<name> := 0.0;        
</tracers>

end;

<tracers vertLoc=WAT; isOutput=1>
  procedure cgt_write_output_to_netcdf_<name>(filename: AnsiString; defineOnly: Boolean);
  begin
    disp('    writing tracer <name>');
    writenc_double(filename,trim('<name>'),output_<name>,'z_axis;time',defineOnly);
    if defineOnly then
    begin
      writenc_attribute(filename,trim('<name>'),'long_name','<description>');
      writenc_attribute(filename,trim('<name>'),'units','mol/kg');
    end;
  end;
</tracers>
<tracers vertLoc/=WAT; isOutput=1>
  procedure cgt_write_output_to_netcdf_<name>(filename: AnsiString; defineOnly: Boolean);
  begin
    disp('    writing tracer <name>');
    writenc_double(filename,trim('<name>'),output_<name>,'time',false,defineOnly);
    if defineOnly then
    begin
      writenc_attribute(filename,trim('<name>'),'long_name','<description>');
      writenc_attribute(filename,trim('<name>'),'units','mol/m**2');  
    end;
  end;
</tracers>
<auxiliaries vertLoc=WAT; isOutput=1>
  procedure cgt_write_output_to_netcdf_<name>(filename: AnsiString; defineOnly: Boolean);
  begin
    disp('    writing auxiliary <name>');
    writenc_double(filename,trim('<name>'),output_<name>,'z_axis;time',defineOnly);
    if defineOnly then
    begin
      writenc_attribute(filename,trim('<name>'),'long_name','<description>');
      writenc_attribute(filename,trim('<name>'),'units','mol/kg');
    end;
  end;
</auxiliaries>
<auxiliaries vertLoc/=WAT; isOutput=1>
  procedure cgt_write_output_to_netcdf_<name>(filename: AnsiString; defineOnly: Boolean);
  begin
    disp('    writing auxiliary <name>');
    writenc_double(filename,trim('<name>'),output_<name>,'time',false,defineOnly);
    if defineOnly then
    begin
      writenc_attribute(filename,trim('<name>'),'long_name','<description>');
      writenc_attribute(filename,trim('<name>'),'units','mol/m**2');  
    end;
  end;
</auxiliaries>
<processes vertLoc=WAT; isOutput=1>
  procedure cgt_write_output_to_netcdf_<name>(filename: AnsiString; defineOnly: Boolean);
  begin
    disp('    writing process <name>');
    writenc_double(filename,trim('<name>'),output_<name>,'z_axis;time',defineOnly);
    if defineOnly then
    begin
      writenc_attribute(filename,trim('<name>'),'long_name','<description>');
      writenc_attribute(filename,trim('<name>'),'units','mol/kg');
    end;
  end;
</processes>
<processes vertLoc/=WAT; isOutput=1>
  procedure cgt_write_output_to_netcdf_<name>(filename: AnsiString; defineOnly: Boolean);
  begin
    disp('    writing process <name>');
    writenc_double(filename,trim('<name>'),output_<name>,'time',false,defineOnly);
    if defineOnly then
    begin 
      writenc_attribute(filename,trim('<name>'),'long_name','<description>');
      writenc_attribute(filename,trim('<name>'),'units','mol/m**2');  
    end;
  end;
</processes>

procedure cgt_write_output_to_netcdf(defineOnly: Boolean);
var
  filename: AnsiString;
begin
  filename:=ExtractFilePath(application.exeName)+'gotland_deep_1d.nc';
  if FileExists(filename) then deletefile(filename);
  writenc_double(filename,'time',time_axis,'',false,defineOnly);
    writenc_attribute(filename,'time','units','days since 1899-12-31 00:00:00.0');
    writenc_attribute(filename,'time','cartesian_axis','T');
  writenc_double(filename,'z_axis',depths,'',false,defineOnly);
    writenc_attribute(filename,'z_axis','units','m');
    writenc_attribute(filename,'z_axis','cartesian_axis','Z');
    writenc_attribute(filename,'z_axis','positive','down');
  writenc_double(filename,'temperature',output_temperature,'z_axis;time',defineOnly);
    writenc_attribute(filename,'temperature','long_name','potential temperature');
    writenc_attribute(filename,'temperature','units','Celsius');
  writenc_double(filename,'salinity',output_salinity,'z_axis;time',defineOnly);
    writenc_attribute(filename,'salinity','long_name','salinity');
    writenc_attribute(filename,'salinity','units','g/kg');
  writenc_double(filename,'opacity',output_opacity,'z_axis;time',defineOnly);
    writenc_attribute(filename,'opacity','long_name','opacity of the water for PAR');
    writenc_attribute(filename,'opacity','units','1/m');
  writenc_double(filename,'light',output_light,'z_axis;time',defineOnly);
    writenc_attribute(filename,'light_at_top','long_name','downward shortwave light flux');
    writenc_attribute(filename,'light_at_top','units','W/m**2');
  writenc_double(filename,'diffusivity',output_diffusivity,'z_axis;time',defineOnly);
    writenc_attribute(filename,'diffusivity','long_name','vertical turbulent diffusivity');
    writenc_attribute(filename,'diffusivity','units','m**2/s');
  writenc_double(filename,'light_at_top',output_light_at_top,'time',false,defineOnly);
    writenc_attribute(filename,'light_at_top','long_name','downward shortwave light flux at sea surface');
    writenc_attribute(filename,'light_at_top','units','W/m**2');
  writenc_double(filename,'zenith_angle',output_zenith_angle,'time',false,defineOnly);
    writenc_attribute(filename,'zenith_angle','long_name','solar zenith angle');
    writenc_attribute(filename,'zenith_angle','units','deg');
  writenc_double(filename,'zenith_angle',output_zenith_angle,'time',false,defineOnly);
    writenc_attribute(filename,'bottom_stress','long_name','shear stress at the sea floor');
    writenc_attribute(filename,'bottom_stress','units','N/m**2');

  <tracers vertLoc=WAT; isOutput=1>
    cgt_write_output_to_netcdf_<name>(filename,defineOnly);
  </tracers>
  <tracers vertLoc/=WAT; isOutput=1>
    cgt_write_output_to_netcdf_<name>(filename,defineOnly);
  </tracers>
  <auxiliaries vertLoc=WAT; isOutput=1>
    cgt_write_output_to_netcdf_<name>(filename,defineOnly);
  </auxiliaries>
  <auxiliaries vertLoc/=WAT; isOutput=1>
    cgt_write_output_to_netcdf_<name>(filename,defineOnly);
  </auxiliaries>
  <processes vertLoc=WAT; isOutput=1>
    cgt_write_output_to_netcdf_<name>(filename,defineOnly);
  </processes>
  <processes vertLoc/=WAT; isOutput=1>
    cgt_write_output_to_netcdf_<name>(filename,defineOnly);
  </processes>
end;


//------------------------------------------------------------
// THIRD PART
// not modified by code generation tool
//------------------------------------------------------------


procedure run(RungeKuttaDepth: Integer=1);
var
  i, k: Integer;
  current_output_date: Real;
  current_output_index: Integer;

  forcing_matrix_temperature,  forcing_matrix_salinity,
  forcing_matrix_light_at_top,  forcing_matrix_bottom_stress,
  forcing_matrix_opacity_water,  forcing_matrix_diffusivity: DoubleArray2d;

  forcing_index_temperature: Integer;
  forcing_index_salinity: Integer;
  forcing_index_light_at_top: Integer;
  forcing_index_bottom_stress: Integer;
  forcing_index_opacity_water: Integer;
  forcing_index_diffusivity: Integer;

  output_count: Integer;

  sun_pos: tSunPosition;
  zenith_angle_under_water, light_path_length: Double;

begin
  disp('initialization');

  fill_ivlev_lookup_table;

  //initialize the date
  current_date        :=start_date;
  current_output_date :=start_date;
  timestep_increment  :=1.0;
  current_output_index:=1; // time index in output matrix
  if timestep_increment=1.0 then
  begin
    max_output_index    :=trunc(repeated_runs*(end_date-start_date)/output_interval);
    setLength(time_axis,max_output_index);
    for i:=0 to max_output_index-1 do
      time_axis[i]:=(i-1)*output_interval+start_date;
  end
  else
  begin
    max_output_index:=100;
    setLength(time_axis,max_output_index);
    for i:=0 to max_output_index-1 do
      time_axis[i]:=i;
  end;

  disp('  loading physical forcing');

  //load cell heights
  loadMatlabMatrix(ExtractFilePath(application.exeName)+'physics\cellheights.txt', cellheights); // cell heights [m]
  kmax := length(cellheights);             // number of vertical layers
  setLength(depths,kmax);
  depths[0]:=cellheights[0];
  for i:=1 to length(cellheights)-1 do
    depths[i]:=depths[i-1]+cellheights[i];
  fill_dz_arrays;

  //load physics
  loadMatlabMatrix(ExtractFilePath(application.exeName)+'physics\temperature.txt',forcing_matrix_temperature);  // temperature [deg_C]
  loadMatlabMatrix(ExtractFilePath(application.exeName)+'physics\salinity.txt',forcing_matrix_salinity);     // salinity [g/kg]
  loadMatlabMatrix(ExtractFilePath(application.exeName)+'physics\light_at_top.txt',forcing_matrix_light_at_top); // downward flux of
                                                                 // shortwave light at sea surface [W/m2]
  loadMatlabMatrix(ExtractFilePath(application.exeName)+'physics\bottom_stress.txt',forcing_matrix_bottom_stress);// bottom stress [N/m2]
  loadMatlabMatrix(ExtractFilePath(application.exeName)+'physics\opacity_water.txt',forcing_matrix_opacity_water);// clear-water opacity [1/m]
  loadMatlabMatrix(ExtractFilePath(application.exeName)+'physics\diffusivity.txt',forcing_matrix_diffusivity);  // turbulent vertical diffusivity [m2/s]

  forcing_index_temperature := 1;
  forcing_index_salinity := 1;
  forcing_index_light_at_top := 1;
  forcing_index_bottom_stress := 1;
  forcing_index_opacity_water := 1;
  forcing_index_diffusivity := 1;

  disp('  loading biological initialization values');

  //load initial tracer concentrations
  cgt_init_tracers;

  // fill the output with zeros or NaNs
  cgt_init_output;

  setlength(output_vector_temperature,kmax); for i:=0 to kmax-1 do output_vector_temperature[i]:=0;
  setlength(output_vector_salinity,kmax);    for i:=0 to kmax-1 do output_vector_salinity[i]:=0;
  setlength(output_vector_opacity,kmax);     for i:=0 to kmax-1 do output_vector_opacity[i]:=0;
  setlength(output_vector_light,kmax);       for i:=0 to kmax-1 do output_vector_light[i]:=0;
  setlength(output_vector_diffusivity,kmax); for i:=0 to kmax-1 do output_vector_diffusivity[i]:=0;
  output_scalar_light_at_top  := 0.0;
  output_scalar_zenith_angle  := 0.0;
  output_scalar_bottom_stress := 0.0;

  setLength(output_temperature   ,kmax,max_output_index);
  setLength(output_salinity      ,kmax,max_output_index);
  setLength(output_opacity       ,kmax,max_output_index);
  setLength(output_light         ,kmax,max_output_index);
  setLength(output_diffusivity   ,kmax,max_output_index);
  setLength(output_light_at_top  ,max_output_index);
  setLength(output_zenith_angle  ,max_output_index);
  setLength(output_bottom_stress ,max_output_index);
  output_count  := 0;

  setLength(forcing_vector_temperature,kmax);
  setLength(forcing_vector_salinity,kmax);
  setLength(forcing_vector_opacity_water,kmax);
  setLength(forcing_vector_opacity_bio,kmax);
  setLength(forcing_vector_opacity,kmax);
  setLength(forcing_vector_diffusivity,kmax);

  disp('starting the run');

  // do the timestep
  while current_date <= repeated_runs*(end_date-start_date)+start_date do
  begin
    // load the physics
    load_forcing(forcing_matrix_temperature,current_date,start_date,end_date, kmax, forcing_index_temperature, forcing_vector_temperature);
    load_forcing(forcing_matrix_salinity,current_date,start_date,end_date, kmax, forcing_index_salinity, forcing_vector_salinity);
    load_forcing(forcing_matrix_opacity_water,current_date,start_date,end_date, kmax, forcing_index_opacity_water, forcing_vector_opacity_water);
    load_forcing(forcing_matrix_diffusivity,current_date,start_date,end_date, kmax, forcing_index_diffusivity, forcing_vector_diffusivity);
    load_forcing(forcing_matrix_light_at_top,current_date,start_date,end_date, kmax, forcing_index_light_at_top, forcing_scalar_light_at_top);
    load_forcing(forcing_matrix_bottom_stress,current_date,start_date,end_date, kmax, forcing_index_bottom_stress, forcing_scalar_bottom_stress);

    // light calculation
    sun_pos := sun_position(current_date,location);
    forcing_scalar_zenith_angle := sun_pos.zenith*pi/180;

    cgt_calc_opacity_bio(forcing_vector_opacity_bio);

    for i:=0 to kmax-1 do
      forcing_vector_opacity[i]   := forcing_vector_opacity_bio[i] + forcing_vector_opacity_water[i];
    setLength(forcing_vector_light,kmax);
    for i:=0 to kmax-1 do
      forcing_vector_light[i]:=0;
    if forcing_scalar_zenith_angle*180/pi < 90 then // daytime
    begin
      zenith_angle_under_water := arcsin(sin(forcing_scalar_zenith_angle*pi/180)/1.33);// consider refraction at sea surface
      forcing_vector_light[0]  := forcing_scalar_light_at_top;
      for k:=1 to kmax do
      begin
        light_path_length := cellheights[k-1]/cos(zenith_angle_under_water);
        if k<kmax then
        begin
          forcing_vector_light[k] := forcing_vector_light[k-1]*exp(-forcing_vector_opacity[k-1]*light_path_length);
          forcing_vector_light[k]   := forcing_vector_light[k-1]*exp(-forcing_vector_opacity[k-1]*light_path_length*0.5);
        end;
      end;
    end;

    // output of physics during this time step
    for i:=0 to kmax-1 do
    begin
      output_vector_temperature[i]   := output_vector_temperature[i]   + forcing_vector_temperature[i];
      output_vector_salinity[i]      := output_vector_salinity[i]      + forcing_vector_salinity[i];
      output_vector_opacity[i]       := output_vector_opacity[i]       + forcing_vector_opacity[i];
      output_vector_light[i]         := output_vector_light[i]         + forcing_vector_light[i];
      output_vector_diffusivity[i]   := output_vector_diffusivity[i]   + forcing_vector_diffusivity[i];
    end;
    output_scalar_light_at_top  := output_scalar_light_at_top  + forcing_scalar_light_at_top;
    output_scalar_zenith_angle  := output_scalar_zenith_angle  + forcing_scalar_zenith_angle;
    output_scalar_bottom_stress := output_scalar_bottom_stress + forcing_scalar_bottom_stress;
    output_count:=output_count+1;

    // do the biology including vertical migration / particle sinking
    if RungeKuttaDepth=1 then //positive Euler forward or positive Euler forward Patankar
      cgt_bio_timestep
    else if RungeKuttaDepth=2 then // positive Runge Kutta or positive Runge Kutta Patankar
    begin
      //Step 1: save initial tracer values
      for i:=0 to kmax-1 do
      begin
<tracers vertLoc=WAT>
        tracer_vector_intermediate_<name>[i]:=tracer_vector_<name>[i];
</tracers>
      end;
<tracers vertLoc/=WAT>
      tracer_scalar_intermediate_<name>:=tracer_scalar_<name>;
</tracers>

      //Step 2: initialize saved process rates
      for i:=0 to kmax-1 do
      begin
<processes vertLoc=WAT>
        saved_rate_<name>[i]:=0.0;
</processes>
      end;
<processes vertLoc/=WAT>
      saved_rate_<name>:=0.0;
</processes>

      //Step 3: Do intermediate timestep
      cgt_bio_timestep_intermediate;

      //Step 4: Calculate the patankar modification factors
      for i:=0 to kmax-1 do
      begin
<tracers vertLoc=WAT>
        patankar_modification_<name>[i]:=tracer_vector_intermediate_<name>[i]/max(tracer_vector_<name>[i],1.0e-30);
</tracers>
      end;
<tracers vertLoc/=WAT>
      patankar_modification_<name>:=tracer_scalar_intermediate_<name>/max(tracer_scalar_<name>,1.0e-30);
</tracers>

      //Step 5: Do another intermediate timestep to get the new process rates
      cgt_bio_timestep_intermediate;

      //Step 6: calculate the average of the process rates
      for i:=0 to kmax-1 do
      begin
<processes vertLoc=WAT>
        saved_rate_<name>[i]:=saved_rate_<name>[i]/2.0;
</processes>
      end;
<processes vertLoc/=WAT>
      saved_rate_<name>:=saved_rate_<name>/2.0;
</processes>      
      
      //Step 7: Reload original tracer values
      for i:=0 to kmax-1 do
      begin
<tracers vertLoc=WAT>
        tracer_vector_<name>[i]:=tracer_vector_intermediate_<name>[i];
</tracers>
      end;
<tracers vertLoc/=WAT>
      tracer_scalar_<name>:=tracer_scalar_intermediate_<name>;
</tracers>

      //Step 8: Do the actual time step
      cgt_bio_timestep(true);

    end;

    // do the vertical mixing
    cgt_mixing_timestep;

    // check if output needs to be saved in final array
    if current_date*(1.0+1.0e-10) >= current_output_date + output_interval then
    begin
        // display current date/time
        disp('  '+DateTimeToStr(current_date));
        // do the output of physics
        for k:=1 to kmax do
        begin
            output_temperature[k-1,current_output_index-1] := output_vector_temperature[k-1] /output_count;
            output_salinity[k-1,current_output_index-1]    := output_vector_salinity[k-1]    /output_count;
            output_opacity[k-1,current_output_index-1]     := output_vector_opacity[k-1]     /output_count;
            output_light[k-1,current_output_index-1]       := output_vector_light[k-1]       /output_count;
            output_diffusivity[k-1,current_output_index-1] := output_vector_diffusivity[k-1] /output_count;
        end;
        output_light_at_top[current_output_index-1]  := output_scalar_light_at_top   /output_count;
        output_bottom_stress[current_output_index-1] := output_scalar_bottom_stress  /output_count;
        output_zenith_angle[current_output_index-1]  := output_scalar_zenith_angle   /output_count;
        // reset temporary physics output values
        for k:=1 to kmax do
        begin
          output_vector_temperature[k-1]   := 0;
          output_vector_salinity[k-1]      := 0;
          output_vector_opacity[k-1]       := 0;
          output_vector_light[k-1]         := 0;
          output_vector_diffusivity[k-1]   := 0;
        end;
        output_scalar_light_at_top  := 0.0;
        output_scalar_zenith_angle  := 0.0;
        output_scalar_bottom_stress := 0.0;
        //do the output of biology
        if current_output_index <= max_output_index then cgt_output_final(current_output_index, output_count);
        //reset output indexes
        output_count  := 0;
        current_output_index := current_output_index + 1;
        current_output_date:=current_output_date+output_interval;
    end;

    // update the current date/time
    current_date := current_date + timestep;
    timestep:=timestep*timestep_increment;
  end;
  disp('calculation ended, writing results to output file');
  cgt_write_output_to_netcdf(false);
  disp('done.');
end;

end.
