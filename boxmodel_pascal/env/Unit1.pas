unit Unit1;

{$MODE Delphi}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, FileUtil;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Button1: TButton;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Edit2: TEdit;
    Label3: TLabel;
    Edit3: TEdit;
    Label4: TLabel;
    Edit4: TEdit;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

uses cgt_1d_model;

procedure configure;
begin
  DecimalSeparator:='.';
  ShortDateFormat:='dd.mm.yyyy';
  DateSeparator:='.';
  cgt_1d_model.start_date         := StrToDate(Form1.Edit1.Text); // initial date
  cgt_1d_model.end_date           := StrToDate(Form1.Edit2.Text); // final date
  cgt_1d_model.repeated_runs      := StrToInt(Form1.Edit3.Text);  // how often the same forcing period is repeated
  cgt_1d_model.timestep           := 1.0/24;              // timestep [days]
  cgt_1d_model.timestep_increment := 1.0;
  cgt_1d_model.output_interval    := StrToFloat(Form1.Edit4.Text); // output interval [days]
  cgt_1d_model.location.longitude := 20.0;                // longitude [deg], for zenith angle calculation
  cgt_1d_model.location.latitude  := 57.33;               // latitude  [deg], for zenith angle calculation
  cgt_1d_model.location.altitude  := 0.0;                 // altitude [m], for zenith angle calculation
  cgt_1d_model.density_water      := 1035.0;              // Density of water [kg/m3] to convert between mol/kg and mol/m3
  cgt_1d_model.num_vmove_steps    := 1;                   // if >1, this splits the vertical movement timestep (keep CFL criterion valid if tracers move very fast)
  cgt_1d_model.num_vdiff_steps    := 10;                  // if >1, this splits the vertical mixing (keep CFL criterion valid if tracers move very fast)
  cgt_1d_model.min_diffusivity    := 1e-4;                // minimum vertical turbulent diffusivity [m2/s]
  cgt_1d_model.max_diffusivity    := 1;                   // maximum vertical turbulent diffusivity [m2/s]
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  if FileExistsUTF8(ExtractFilePath(application.ExeName)+'gotland_deep_1d.nc') { *Converted from FileExists*  } then
    if DeleteFileUTF8(ExtractFilePath(application.ExeName)+'gotland_deep_1d.nc') { *Converted from DeleteFile*  }=false then
    begin
      showMessage('Output file exists and could not be deleted. Delete it first.');
      exit;
    end;
  Memo1.Lines.Clear;
  configure;
  run;
end;

end.
