%-----------------------------
% initialize the output arrays
%-----------------------------

% auxiliary variables
<auxiliaries isFlat=0; isOutput=1>
  % <description> :
    output_<name> = zeros(kmax,max_output_index)/0.0; 
    output_vector_<name> = zeros(1,kmax);
</auxiliaries>
<auxiliaries isFlat=1; isOutput=1>
  % <description> :
    output_<name> = zeros(1   ,max_output_index)/0.0;
    output_scalar_<name> = 0.0;
</auxiliaries>
<auxiliaries isFlat=2; isOutput=1>
  % <description>
    output_<name> = zeros(1   ,max_output_index)/0.0;
    output_scalar_<name> = 0.0;
</auxiliaries>

% tracers
<tracers isFlat=0; isOutput=1>
  % <description> :
    output_<name> = zeros(kmax,max_output_index)/0.0; 
    output_vector_<name> = zeros(1,kmax);
</tracers>
<tracers isFlat=1; isOutput=1>
  % <description> :
    output_<name> = zeros(1   ,max_output_index)/0.0;
    output_scalar_<name> = 0.0;
</tracers>
<tracers isFlat=2; isOutput=1>
  % <description>
    output_<name> = zeros(1   ,max_output_index)/0.0;
    output_scalar_<name> = 0.0;
</tracers>

% processes
<processes isFlat=0; isOutput=1>
  % <description> :
    output_<name> = zeros(kmax,max_output_index)/0.0; 
    output_vector_<name> = zeros(1,kmax);
</processes>
<processes isFlat=1; isOutput=1>
  % <description> :
    output_<name> = zeros(1   ,max_output_index)/0.0;
    output_scalar_<name> = 0.0;
</processes>
<processes isFlat=2; isOutput=1>
  % <description>
    output_<name> = zeros(1   ,max_output_index)/0.0;
    output_scalar_<name> = 0.0;
</processes>